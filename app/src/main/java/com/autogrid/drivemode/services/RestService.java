package com.autogrid.drivemode.services;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Krishna on 6/11/2016.
 */
public class RestService {

    private String BASE_REST_URL = "http://35.154.158.32:1500";

    public RestService(){}

    public  String restServiceCall(String url, String method, JSONObject data) {

        Log.d("RestService", "WebService Url - "+url+"   Data - "+data);
        String response = null;
        try{
            URL restUrl = new URL(BASE_REST_URL+url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) restUrl.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setRequestMethod(method);

            OutputStream wr= httpURLConnection.getOutputStream();
            wr.write(data.toString().getBytes("UTF-8"));
            wr.close();

            Log.d(RestService.class.getSimpleName(), "Rest Call:-"+restUrl);
            int responseCode = httpURLConnection.getResponseCode();
            Log.d(RestService.class.getSimpleName(), "Responce code is:-"+responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer responsBuffer = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                responsBuffer.append(inputLine);
            }
            in.close();

            return responsBuffer.toString();
        }catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
    }

    protected  String restServiceCall(String url, String method, JSONArray data) {

        String response = null;
        try{
            URL restUrl = new URL(BASE_REST_URL+url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) restUrl.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setRequestMethod(method);

            OutputStream wr= httpURLConnection.getOutputStream();
            wr.write(data.toString().getBytes("UTF-8"));
            wr.close();

            Log.d(RestService.class.getSimpleName(), "Rest Call:-"+restUrl);
            int responseCode = httpURLConnection.getResponseCode();
            Log.d(RestService.class.getSimpleName(), "Responce code is:-"+responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer responsBuffer = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                responsBuffer.append(inputLine);
            }
            in.close();

            return responsBuffer.toString();
        }catch (java.io.FileNotFoundException e){
            return "{}";
        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    protected String restServiceCall(String url, String method, String data) {

        return null;
    }

    protected String restServiceCall(String url, String method) {

        try {
            URL urlObj = new URL(BASE_REST_URL+url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObj.openConnection();

            httpURLConnection.setRequestMethod(method);

            int responseCode = httpURLConnection.getResponseCode();

            Log.d(RestService.class.getSimpleName(), "Response code:-"+responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String externalRestServiceCall(String url, String method) {

        try {
            URL urlObj = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObj.openConnection();

            httpURLConnection.setRequestMethod(method);

            int responseCode = httpURLConnection.getResponseCode();

            Log.d(RestService.class.getSimpleName(), "Response code:-"+responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    protected String restServiceCall(String fileName) {

        try {
            URL urlObj = new URL(BASE_REST_URL+"/roadconditions/upload_csv_file");
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObj.openConnection();

            httpURLConnection.setRequestMethod("POST");


            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoOutput(true); // indicates POST method
            httpURLConnection.setDoInput(true);

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Cache-Control", "no-cache");
            //httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);


            int responseCode = httpURLConnection.getResponseCode();

            Log.d(RestService.class.getSimpleName(), "Response code:-"+responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
               DataOutputStream request = new DataOutputStream(httpURLConnection.getOutputStream()); response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
