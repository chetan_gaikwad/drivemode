package com.autogrid.drivemode.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.autogrid.drivemode.db.entity.Trip;
import com.autogrid.drivemode.db.entity.TripStatus;
import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.util.BusProvider;
import com.autogrid.drivemode.util.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Service is used to initialize location and provide to app where ever required
 */
public class LocationManagerService extends Service implements LocationListener {

    @Inject
    LocationManager locationManager;
    @Inject
    SharedPreferences preferences;

    private int mMaxSpeed;
    private Trip trip;
    private Location mLocation;
    private float mTripDistanceCounter;
    private Location mLastPosition;
    private int distanceTimeConstant = 10000;
    private int mTimeConstant = 1000;
    private int mSpeedConstant = 3000;
    private Handler distanceCalculatorHandler;
    private Handler timerHandler;
    private Handler speedHandler;
    private boolean mIsRunning;
    private int startTime = 0;
    private TripStatus tripStatus;

    @Override
    public void onCreate() {
        ((DriveModeApp)getApplication()).getDriveModeComponent().inject(this);
        initializeLocation();
        tripStatus = new TripStatus();
        distanceCalculatorHandler = new Handler();
        timerHandler = new Handler();
        speedHandler = new Handler();
        mIsRunning = false;
        //Distance counter thread
        distanceCalculatorHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Timber.d("Calculating distance");
                if(mLastPosition != null && mLocation != null){
                    float distance = mLastPosition.distanceTo(mLocation);
                    mTripDistanceCounter = mTripDistanceCounter + distance;
                    mLastPosition = mLocation;
                } else if(mLastPosition == null && mLocation != null){
                    mLastPosition = mLocation;
                }
                distanceCalculatorHandler.postDelayed(this, distanceTimeConstant);
            }
        }, distanceTimeConstant);

        //timer thread
        timerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startTime = startTime + 1;
                float distance = mTripDistanceCounter == 0 ? 0:mTripDistanceCounter/1000;
                tripStatus.setDistance(String.format(Locale.US,"%02.02f km", distance));

                float hours = startTime / 3600;
                float minutes = (startTime % 3600) / 60;
                float seconds = startTime % 60;


                tripStatus.setTime(String.format(Locale.US, "%02.0f:%02.0f:%02.0f", hours, minutes, seconds));
                BusProvider.getInstance().post(tripStatus);
                distanceCalculatorHandler.postDelayed(this, mTimeConstant);

            }
        }, mTimeConstant);

        //speed thread
        speedHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Timber.d("Calculating speed");
                if(mLastPosition != null && mLocation != null) {
                    float distance = mLastPosition.distanceTo(mLocation);
                    double mps = (distance/3.0);
                    double coverter = (double) 18/5;
                    double speed = mps*coverter;

                    Timber.d("mps %f converter %s speed %f", mps,coverter,speed);
                    tripStatus.setSpeed(String.format(Locale.US,"%02.02f km/h",speed));
                    BusProvider.getInstance().post(tripStatus);
                }
                speedHandler.postDelayed(this, mSpeedConstant);
            }
        }, mSpeedConstant);
    }


    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.d("Location service started");
        mIsRunning = true;
        startTrip(intent);
        return Service.START_STICKY;
    }


    /**
     * start trip and take initial parameters
     * @param intent
     */
    private void startTrip(Intent intent) {
        trip = new Trip();
        trip.setStartTime(getTimeMicro());
        String tripName = preferences.getString(Utils.TRIP_NAME,"trip#");
        Timber.d("Trip name %s",tripName);
        trip.setTripName(tripName);
    }

    /**
     * Return if service is running or not
     */
    boolean isRunning(){
        return mIsRunning;
    }
    private void initializeLocation() {

        Criteria criteria = new Criteria();
        criteria.setSpeedRequired(true);
        String bestProvider = locationManager.getBestProvider(criteria, true);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(bestProvider, 250, 5, this);
        }
    }

    public void onLocationChanged(Location location) {
        mLocation = location;
        BusProvider.getInstance().post(location);

        //if start lat,long not set yet then set it(Often will face this
        // when you start trip in non GPS area like home or parking)
        if(trip.getStartLat() == 0 && trip.getStartLong() == 0){
            trip.setStartLat(location.getLatitude());
            trip.setStartLong(location.getLongitude());
            fetchStartAddress();
        }
        if(location.getSpeed() > mMaxSpeed){
            mMaxSpeed = (int) location.getSpeed();
        }
    }

    /**
     * Fetch start address from co-ordinates
     */
    private void fetchStartAddress() {
        try{
            if(Utils.isOnline(this) && trip.getStartLat() != 0){
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(LocationManagerService.this, Locale.getDefault());

                addresses = geocoder.getFromLocation(trip.getStartLat(), trip.getStartLong(), 1);
                trip.setStartAddress(addresses.get(0).getAddressLine(0)
                        + addresses.get(0).getAddressLine(1));
            }
        } catch (IOException e) {
            Timber.d("Error while getting start Address");
            e.printStackTrace();
        }
    }

    /**
     * Fetch stop address from co-ordinates
     */
    private void fetchStopAddress() {
        try{
            if(Utils.isOnline(this) && trip.getStopLat()!= 0 ){
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(LocationManagerService.this, Locale.getDefault());

                addresses = geocoder.getFromLocation(trip.getStopLat(), trip.getStopLong(), 1);
                trip.setStopAddress(addresses.get(0).getAddressLine(0)
                        + addresses.get(0).getAddressLine(1));
            }
        } catch (IOException e) {
            Timber.d("Error while getting stop Address");
            e.printStackTrace();
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onDestroy() {
        Timber.d("Location service stopped");
        stopTrip();
        BusProvider.getInstance().post(trip);
        distanceCalculatorHandler.removeMessages(0);
        timerHandler.removeMessages(0);
        speedHandler.removeMessages(0);
    }

    /**
     * Save trip and send to Dashboard
     */
    private void stopTrip() {
        try {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                stopGps();
            }
            trip.setStopLat(mLocation == null ? 0:mLocation.getLatitude());
            trip.setStopLong(mLocation == null ? 0:mLocation.getLongitude());
            trip.setStopTime(getTimeMicro());

            fetchStopAddress();

            trip.setTotalTimeTaken(calculateDuration(trip));
            trip.setTotalDistanceCovered(mTripDistanceCounter == 0 ? 0:mTripDistanceCounter/1000);
            trip.setMaxSpeed(mMaxSpeed);
            if(mTripDistanceCounter > 0.0) {
                long timeTakenInMill = (dateStringToMilli(trip.getStopTime())-dateStringToMilli(trip.getStartTime()));
                Log.d("fref","Timple "+timeTakenInMill);

                float avgSpeed = ((mTripDistanceCounter / 1000) / ((float) (timeTakenInMill/1000) / ( 60 * 60))  ) ;
                trip.setAvgSpeed(String.format(Locale.US, "%.2f ", avgSpeed));
            } else {
                trip.setAvgSpeed(String.format(Locale.US, "%.2f ", 0.00));
            }

            saveTripToDb();
        } catch (Exception w) {
            w.printStackTrace();
        }
    }



    /**
     * Save trip to database
     */
    private void saveTripToDb() {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.TRIPS).push();
        mDatabase.setValue(trip);
    }

    /**
     * Remove GPS listener
     */
    private void stopGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(this);
        locationManager = null;
    }

    /**
     * Calculating trip duration
     * @param trip
     * @return
     */
    private String calculateDuration(Trip trip) {
        long diff = (dateStringToMilli(trip.getStopTime())-dateStringToMilli(trip.getStartTime()));
        return formatTime(diff);
    }

    private String formatTime(long diff) {
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        System.out.print(diffDays + " days, ");
        System.out.print(diffHours + " hours, ");
        System.out.print(diffMinutes + " minutes, ");
        System.out.print(diffSeconds + " seconds.");

        Log.d("Date difference:-", " Days:" + diffDays + " Hours:"+diffHours + " Minutes:" + diffMinutes + " Second:" + diffSeconds);
        StringBuilder dateDiff = new StringBuilder();

        if(diffDays > 0) {
            dateDiff.append(String.format(Locale.US, "%02d:", diffDays));
        }
        if(diffHours > 0 ) {
            dateDiff.append(String.format(Locale.US, "%02d:", diffHours));
        } else {
            dateDiff.append("00:");
        }
        if(diffMinutes > 0 ) {
            dateDiff.append(String.format(Locale.US, "%02d:", diffMinutes));
        } else {
            dateDiff.append("00:");
        }
        if(diffSeconds > 0 ) {
            dateDiff.append(String.format(Locale.US, "%02d", diffSeconds));
        } else {
            dateDiff.append("00");
        }

        return dateDiff.toString();
    }


    /**
     * get date in yyyy-MM-dd HH:mm:ss format
     * @return
     */
    public static String getTimeMicro() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return sdf.format(c.getTime());
    }

    /**
     * Function to convert date string into milliseconds
     * @param dateString
     * @return
     */
    public long dateStringToMilli(String dateString){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));


        try {
            Date date = sdf.parse(dateString);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}

