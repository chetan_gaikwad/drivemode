package com.autogrid.drivemode.video_blogs;

import com.autogrid.drivemode.db.entity.SosContacts;

import java.util.List;

/**
 * Created by chetan_g on 22/11/16.
 */

interface VideoBlogsContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
}
