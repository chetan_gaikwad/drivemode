package com.autogrid.drivemode.di.component;

import com.autogrid.drivemode.dashboard.MainActivity;
import com.autogrid.drivemode.di.model.AppModule;
import com.autogrid.drivemode.di.model.NetModule;
import com.autogrid.drivemode.manage_trips.live_trip.LiveTripActivity;
import com.autogrid.drivemode.manage_trips.trip_list.TripListActivity;
import com.autogrid.drivemode.profile.ProfileActivity;
import com.autogrid.drivemode.services.LocationManagerService;
import com.autogrid.drivemode.settings.SettingFragment;
import com.autogrid.drivemode.sos.CleanerOverlay;
import com.autogrid.drivemode.sos.ScreenStateReceiver;
import com.autogrid.drivemode.sos.SosActivity;
import com.autogrid.drivemode.user_signin.UserSignInActivity;
import com.autogrid.drivemode.user_signin.UserSignUpActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface DriveModeComponent {
    void inject(LocationManagerService locationManagerService);

    void inject(UserSignInActivity userSignInActivity);

    void inject(TripListActivity tripListActivity);

    void inject(MainActivity mainActivity);

    void inject(SosActivity sosActivity);

    void inject(SettingFragment settingFragment);

    void inject(ScreenStateReceiver screenStateReciever);

    void inject(CleanerOverlay cleanerOverlay);

    void inject(UserSignUpActivity userSignUpActivity);

    void inject(ProfileActivity profileActivity);

    void inject(LiveTripActivity liveTripActivity);
}