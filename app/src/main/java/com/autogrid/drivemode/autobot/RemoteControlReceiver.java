package com.autogrid.drivemode.autobot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.autogrid.drivemode.util.BusProvider;


/**
 * This class will handle all the A2DP and headphone events
 */
public class RemoteControlReceiver extends BroadcastReceiver {
    static int d = 0;

    private Context context;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("hhhhjh", "received intent for action : " + intent.getAction());
        this.context = context;
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
        }


        final KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        if (event.getAction() != KeyEvent.ACTION_DOWN) return;

        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_STOP:
                Toast.makeText(context, "A2dp is stop", Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_HEADSETHOOK:

                d++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        // single click *******************************
                        if (d == 1) {
                            Toast.makeText(context, "single click!", Toast.LENGTH_SHORT).show();
                        }
                        d = 0;
                    }
                };
                if (d == 1) {
                    handler.postDelayed(r, 4000);
                }

                BusProvider.getInstance().post(Events.HEADPHONE_CLICK);
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                break;
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                Toast.makeText(context, "A2dp is next", Toast.LENGTH_SHORT).show();
                BusProvider.getInstance().post("Nexth");
                break;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                Toast.makeText(context, "A2dp is previous", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}