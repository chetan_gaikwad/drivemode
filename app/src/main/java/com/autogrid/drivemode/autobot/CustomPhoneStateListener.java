package com.autogrid.drivemode.autobot;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import timber.log.Timber;

public class CustomPhoneStateListener extends PhoneStateListener {

    public int phoneState = 1000;
    //private static final String TAG = "PhoneStateChanged";
    Context context; //Context to make Toast if required 
    public CustomPhoneStateListener(Context context) {
        super();
        this.context = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);

        switch (state) {
        case TelephonyManager.CALL_STATE_IDLE:
            phoneState = TelephonyManager.CALL_STATE_IDLE;
            //when Idle i.e no call
            Timber.d("Phone state Idle");
            break;
        case TelephonyManager.CALL_STATE_OFFHOOK:
            phoneState = TelephonyManager.CALL_STATE_OFFHOOK;
            //when Off hook i.e in call
            //Make intent and start your service here
            Timber.d("Phone state Off hook");
            break;
        case TelephonyManager.CALL_STATE_RINGING:
            phoneState = TelephonyManager.CALL_STATE_RINGING;
            //when Ringing
            Timber.d("Phone state Ringing");
            break;
        default:
            break;
        }
    }
}