package com.autogrid.drivemode.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.autogrid.drivemode.util.FontCache;


/**
 * Created by one on 3/12/15.
 */
public class MyTextViewOpenSansSemibold extends AppCompatTextView {

    public MyTextViewOpenSansSemibold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewOpenSansSemibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewOpenSansSemibold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Regular.ttf");
            Typeface tf = FontCache.get("fonts/OpenSans-Semibold.ttf", getContext());
            setTypeface(tf);
        }
    }

}