package com.autogrid.drivemode.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.autogrid.drivemode.util.FontCache;

/**
 * Created by Krishna on 1/3/2018.
 */

public class MyTextViewOpenSansRegular extends AppCompatTextView {


    public MyTextViewOpenSansRegular(Context context) {
        super(context);
        init();
    }

    public MyTextViewOpenSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyTextViewOpenSansRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Regular.ttf");
            Typeface tf = FontCache.get("fonts/OpenSans-Regular.ttf", getContext());
            setTypeface(tf);
        }
    }
}
