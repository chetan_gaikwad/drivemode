package com.autogrid.drivemode.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.autogrid.drivemode.util.FontCache;


/**
 * Created by one on 3/12/15.
 */
public class MyTextViewOpenSansBold extends AppCompatTextView {

    public MyTextViewOpenSansBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewOpenSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewOpenSansBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Regular.ttf");
            Typeface tf = FontCache.get("fonts/OpenSans-Bold.ttf", getContext());
            setTypeface(tf);
        }
    }
}