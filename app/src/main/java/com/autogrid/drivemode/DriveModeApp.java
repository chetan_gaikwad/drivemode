package com.autogrid.drivemode;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.autogrid.drivemode.autobot.SettingsManager;
import com.autogrid.drivemode.di.component.DaggerDriveModeComponent;
import com.autogrid.drivemode.di.component.DriveModeComponent;
import com.autogrid.drivemode.di.model.AppModule;
import com.autogrid.drivemode.di.model.NetModule;
import com.autogrid.drivemode.sos.ScreenStateReceiver;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.leakcanary.LeakCanary;

import ai.api.util.BluetoothController;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;


/**
 * Created by Chetan on 2/7/2017.
 */

public class DriveModeApp extends Application {
    private DriveModeComponent driveModeComponent;

    private int activitiesCount;
    private BluetoothControllerImpl bluetoothController;
    private SettingsManager settingsManager;
    private ScreenStateReceiver mScreenStateReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(DriveModeApp.this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        bluetoothController = new BluetoothControllerImpl(this);
        settingsManager = new SettingsManager(this);

        //Initialize LeakCanary start
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        //Initialize leakcanary finish

        //Initialize Dagger2 component start
        driveModeComponent = DaggerDriveModeComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.BASE_URL))
                .build();

        //Initialize Dagger2 component end

        //Initialize Timber logger
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            //Timber.plant(new CrashReportingTree());
        }
        //Initialize Timber logger end


//initializeNavDrawer(toolbar);
        mScreenStateReceiver = new ScreenStateReceiver();
        registerScreenStateReceiver();
    }

    private void registerScreenStateReceiver() {
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, screenStateFilter);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterScreenStateReceiver();//Unregister screen lock listener
    }

    private void unregisterScreenStateReceiver() {
        if(mScreenStateReceiver != null){
            unregisterReceiver(mScreenStateReceiver);
        }
    }

    public DriveModeComponent getDriveModeComponent(){
        return driveModeComponent;
    }

    public void onActivityResume() {
        if (activitiesCount++ == 0) { // on become foreground
            if (settingsManager.isUseBluetooth()) {
                bluetoothController.start();
            }
        }
    }

    public void onActivityPaused() {
        if (--activitiesCount == 0) { // on become background
            bluetoothController.stop();
        }
    }

    private boolean isInForeground() {
        return activitiesCount > 0;
    }

    private class BluetoothControllerImpl extends BluetoothController {

        public BluetoothControllerImpl(Context context) {
            super(context);
        }

        @Override
        public void onHeadsetDisconnected() {
            Log.d("appl", "Bluetooth headset disconnected");
        }

        @Override
        public void onHeadsetConnected() {
            Log.d("Appl", "Bluetooth headset connected");

            if (isInForeground() && settingsManager.isUseBluetooth()
                    && !bluetoothController.isOnHeadsetSco()) {
                bluetoothController.start();
            }
        }

        @Override
        public void onScoAudioDisconnected() {
            Log.d("Appl", "Bluetooth sco audio finished");
            bluetoothController.stop();

            if (isInForeground() && settingsManager.isUseBluetooth()) {
                bluetoothController.start();
            }
        }

        @Override
        public void onScoAudioConnected() {
            Log.d("Appl", "Bluetooth sco audio started");
        }
    }

    public BluetoothController getBluetoothController() {
        return bluetoothController;
    }
}
