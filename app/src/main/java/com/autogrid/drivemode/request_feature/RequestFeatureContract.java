package com.autogrid.drivemode.request_feature;

import com.autogrid.drivemode.db.entity.Profile;

/**
 * Created by chetan_g on 22/11/16.
 */

interface RequestFeatureContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
}
