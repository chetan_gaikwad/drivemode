package com.autogrid.drivemode.request_feature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.RequestFeature;
import com.autogrid.drivemode.util.Utils;
import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;


public class RequestFeatureActivity extends BaseActivity implements RequestFeatureContract {

    @BindView(R.id.edt_request)
    EditText edtRequest;
    @BindView(R.id.txt_request)
    TextView txtRequest;
    private RequestFeaturePresenter requestFeaturePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_feature);
        requestFeaturePresenter = new RequestFeaturePresenter(this);
        ButterKnife.bind(this);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.txt_request)
    void onClick(View view){
        if(!edtRequest.getText().toString().isEmpty()) {
            requestFeaturePresenter.sendRequest(
                    new RequestFeature(Utils.getTimeMicro(),
                    FirebaseAuth.getInstance().getCurrentUser().getEmail() == null?"":
                            FirebaseAuth.getInstance().getCurrentUser().getEmail(),
                    edtRequest.getText().toString()));
        } else {
            onError("Request cannot be empty");
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        Toasty.success(this, successMessage).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toasty.error(this, errorMessage).show();
    }


}
