package com.autogrid.drivemode.request_feature;

import android.support.annotation.NonNull;

import com.autogrid.drivemode.db.entity.RequestFeature;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import es.dmoral.toasty.Toasty;

/**
 * Created by chetan_g on 22/11/16.
 */

class RequestFeaturePresenter {

    private RequestFeatureActivity view;
    RequestFeaturePresenter(RequestFeatureActivity view) {
        this.view = view;
    }

    void removeView(){
        view = null;
    }

    void sendRequest(RequestFeature request) {

        view.showProgressDialog();
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child(Utils.APP_NAME)
                .child(Utils.FEATURE_REQUEST)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .push();
        mDatabase.setValue(request).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    view.onSuccess("Feature request sent");
                    view.onBackPressed();
                } else {
                    view.onError("Error");
                }

                view.hideProgressDialog();
            }
        });
    }
}
