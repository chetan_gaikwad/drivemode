package com.autogrid.drivemode.sos;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.SosContacts;
import com.autogrid.drivemode.util.RuntimePermissionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

public class SosActivity extends BaseActivity implements SosContract {

    private static final int PICK_CONTACT = 100;

    private final int CONTACT_PERMISSION_CODE = 23;

    @BindView(R.id.sosList)
    RecyclerView tripList;
    private SosListAdapter adapter;
    private SosPresenter sosPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);

        ((DriveModeApp)getApplication()).getDriveModeComponent().inject(this);
        ButterKnife.bind(this);
        intializeTripListRecycleView();
        sosPresenter = new SosPresenter(this);
        sosPresenter.fetchAllContactsFromDatabase();

        if(RuntimePermissionManager.isPermissionAllowed(this, Manifest.permission.READ_CONTACTS)) {
            Timber.d("Have contact permission");
        } else {
            RuntimePermissionManager.requestPermission(this, Manifest.permission.READ_CONTACTS, CONTACT_PERMISSION_CODE);
        }

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void intializeTripListRecycleView() {
        tripList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        tripList.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccess(String successMessage) {
        Toasty.success(this, successMessage).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toasty.error(this, errorMessage).show();
    }

    @Override
    public void startActivity() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_contacts) {
            Uri uri = Uri.parse("content://contacts");
            Intent intent = new Intent(Intent.ACTION_PICK, uri);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, PICK_CONTACT);
            return true;
        } else if(id == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSosContactList(List<SosContacts> trips) {
        adapter = new SosListAdapter(SosActivity.this, trips);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tripList.setAdapter(adapter);
            }
        });
    }

    public void deleteTrip(final SosContacts trip) {

        new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Do you want delete this contact from Emergency list?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        new AsyncTask<Void, Void, Integer>() {
                            @Override
                            protected Integer doInBackground(Void... params) {
                                sosPresenter.deleteContact(trip);
                                return 0;
                            }

                            @Override
                            protected void onPostExecute(Integer integer) {

                                super.onPostExecute(integer);
                            }
                        }.execute();

                        adapter.notifyDataSetChanged();
                    }})
                .setNegativeButton(android.R.string.no, null).show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == PICK_CONTACT) {
            if (resultCode == RESULT_OK) {
                Uri uri = intent.getData();
                String[] projection = { ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

                Cursor cursor = getContentResolver().query(uri, projection,
                        null, null, null);
                cursor.moveToFirst();

                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex);

                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(nameColumnIndex);

                Timber.d("ZZZ number : " + number +" , name : "+name);

                SosContacts sosContacts = new SosContacts();
                sosContacts.setSosName(name);
                sosContacts.setSosNumber(number);
                sosPresenter.saveContactToDb(sosContacts);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sosPresenter.removeView();
    }
}
