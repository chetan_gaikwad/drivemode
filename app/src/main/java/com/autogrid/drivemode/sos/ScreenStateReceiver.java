package com.autogrid.drivemode.sos;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.db.entity.SosContacts;
import com.autogrid.drivemode.util.PreferenceUtil;
import com.autogrid.drivemode.util.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

import static android.content.Context.NOTIFICATION_SERVICE;

public class ScreenStateReceiver extends BroadcastReceiver {

    @Inject
    SharedPreferences preferences;

    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        (((DriveModeApp)context.getApplicationContext())).getDriveModeComponent().inject(this);

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.i("Check","Screen went OFF");
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT &&
                    (preferences.getBoolean(PreferenceUtil.LOGIN_PREFERENCE, false))){
                /*setPersonalDataNotification();
                setSosNotification();*/
            } else {
                if (canDrawOverOtherApps(context)) {
                    if ((preferences.getBoolean(PreferenceUtil.LOCK_WIDGET_PREFERENCE, false)) &&
                                    (preferences.getBoolean(PreferenceUtil.LOGIN_PREFERENCE, false))) {

                        CleanerOverlay cleanerOverlay = new CleanerOverlay(context);
                        cleanerOverlay.show();
                    }

                } else {
                    Log.d("casd", "Cannot draw");
                }

                if(preferences.getBoolean(PreferenceUtil.NOTIFICATION_WIDGET_PREFERENCE, false)){
                    Log.d("casd", "Preference not set to draw widget");

                    DatabaseReference mDatabase;
                    mDatabase = FirebaseDatabase.getInstance().getReference();

                    ValueEventListener postListener2 = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Timber.d(dataSnapshot.toString());
                            Profile profile = dataSnapshot.getValue(Profile.class);

                            customNotification(
                                    0,
                                    profile.getName(),
                                    "Blood Group: "+profile.getBloodGroup(),
                                    "Medical Condition: "+profile.getOnGoingMedications());


                            if(profile.getInsuranceCoName() != null && !profile.getInsuranceCoName().isEmpty()) {

                                customNotification(
                                        3,
                                        "Insurance Information",
                                        "Company: "+profile.getInsuranceCoName(),
                                        "Policy number: "+profile.getPolicyNumber());
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Getting Post failed, log a message
                            Timber.d("loadPost:onCancelled", databaseError.toException());
                            // ...
                        }
                    };
                    mDatabase.child(Utils.APP_NAME)
                            .child(Utils.USERS)
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child(Utils.PROFILE)
                            .addValueEventListener(postListener2);

                    ValueEventListener postListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Timber.d("Contacts %s", dataSnapshot.toString());
                            List<SosContacts> contactsList = new ArrayList<>();
                            for (DataSnapshot contactSnapshot: dataSnapshot.getChildren()) {
                                SosContacts sosContacts = contactSnapshot.getValue(SosContacts.class);
                                sosContacts.setSosContactId(contactSnapshot.getKey());
                                contactsList.add(sosContacts);
                            }
                            showSosContactList(contactsList);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Getting Post failed, log a message
                            Timber.d("loadPost:onCancelled", databaseError.toException());
                            // ...
                        }
                    };
                    mDatabase.child(Utils.APP_NAME)
                            .child(Utils.USERS)
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child(Utils.CONTACTS)
                            .addValueEventListener(postListener);
                }

                //setPersonalDataNotification();
                //setSosNotification();
            }
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.i("Check","Screen went ON");
        }
    }

    private void setSosNotification() {

        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
              /*  SosDao sosDao = db.sosDao();
                List<SosContacts> userContacts = sosDao.getAllSosContacts();

                customNotification(
                        1,
                        userContacts.size() >=1 ? "SOS - "+userContacts.get(0).getSosName()+" "+userContacts.get(0).getSosNumber():"Please add sos contacts",
                        userContacts.size() >= 2 ? "SOS - "+userContacts.get(1).getSosName()+" "+userContacts.get(1).getSosNumber():"",
                        userContacts.size() >= 3 ? "SOS - "+userContacts.get(2).getSosName()+" "+userContacts.get(2).getSosNumber():"");
*/
                return 0;
            }

            @Override
            protected void onPostExecute(Integer integer) {

                super.onPostExecute(integer);
            }
        }.execute();

    }

    private void setPersonalDataNotification() {
     /*   UserEntity user = (UserEntity) userRepo.findAll().get(0);
        String bloodGroup = "";
        if(user.getBloodGrp() != null) {
            switch (user.getBloodGrp()) {
                case 0:
                    bloodGroup = "A+";
                    break;
                case 1:
                    bloodGroup = "A-";
                    break;
                case 2:
                    bloodGroup = "B+";
                    break;
                case 3:
                    bloodGroup = "B-";
                    break;
                case 4:
                    bloodGroup = "O+";
                    break;
                case 5:
                    bloodGroup = "O-";
                    break;
                case 6:
                    bloodGroup = "AB+";
                    break;
                case 7:
                    bloodGroup = "AB-";
                    break;
            }
        }*/

    }

    @SuppressLint("NewApi")
    public static boolean canDrawOverOtherApps(Context context) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.N || Settings.canDrawOverlays(context);
    }

    /*public void customNotification(int notificationId, String title, String data, String data3) {
        Log.i("customNotification","Screen went OFF. Showing custom notification");
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.noti_dialog);


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", "SOS Contact "+title);
        intent.putExtra("text", "SOS Contact "+data);
        intent.putExtra("text2", "SOS Contact "+data3);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.mipmap.ic_launcher)
                // Set Ticker Message
                .setTicker("Sticker")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews)
                .setOngoing(true);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.image,R.mipmap.ic_launcher);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title, title);
        remoteViews.setTextViewText(R.id.text,data);
        remoteViews.setTextViewText(R.id.text2,data3);

        // Create Notification Manager
        NotificationManager notificationmanager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(notificationId, builder.build());

    }*/

    public void customNotification(int notificationId, String title, String data, String data3) {
        Log.i("customNotification","Screen went OFF. Showing custom notification");
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.noti_dialog);


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", title);
        intent.putExtra("text", data);
        intent.putExtra("text2", data3);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.mipmap.new_icon)
                // Set Ticker Message
                .setTicker("Sticker")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews)
                .setOngoing(true);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.image,R.mipmap.new_icon);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title,title);
        remoteViews.setTextViewText(R.id.text,data);
        remoteViews.setTextViewText(R.id.text2,data3);


        // Create Notification Manager
        NotificationManager notificationmanager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(notificationId, builder.build());

        /** Check is notification enables */
        if(preferences.getBoolean(PreferenceUtil.NOTIFICATION_WIDGET_PREFERENCE, false)) {


        }
    }

    private void showSosContactList(List<SosContacts> userContacts) {

        customNotification(
                1,
                userContacts.size() >=1 ? "SOS: "+userContacts.get(0).getSosName()+"-"+userContacts.get(0).getSosNumber():"Please add sos contacts",
                userContacts.size() >= 2 ? "SOS: "+userContacts.get(1).getSosName()+"-"+userContacts.get(1).getSosNumber():"",
                userContacts.size() >= 3 ? "SOS: "+userContacts.get(2).getSosName()+"-"+userContacts.get(2).getSosNumber():"");

    }
}