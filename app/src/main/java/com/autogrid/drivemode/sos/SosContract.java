package com.autogrid.drivemode.sos;

import com.autogrid.drivemode.db.entity.SosContacts;
import com.autogrid.drivemode.db.entity.Trip;

import java.util.List;

/**
 * Created by chetan_g on 22/11/16.
 */

interface SosContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
     void showSosContactList(List<SosContacts> trips);
}
