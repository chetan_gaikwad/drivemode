package com.autogrid.drivemode.sos;

import android.support.annotation.NonNull;

import com.autogrid.drivemode.db.entity.SosContacts;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by chetan_g on 22/11/16.
 */

class SosPresenter {

    private SosActivity view;

    SosPresenter(SosActivity view) {
        this.view = view;
    }

    void fetchAllContactsFromDatabase(){
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        view.showProgressDialog();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d("Contacts %s", dataSnapshot.toString());
                List<SosContacts> contactsList = new ArrayList<>();
                for (DataSnapshot contactSnapshot: dataSnapshot.getChildren()) {
                    SosContacts sosContacts = contactSnapshot.getValue(SosContacts.class);
                    sosContacts.setSosContactId(contactSnapshot.getKey());
                    contactsList.add(sosContacts);
                }

                if(view != null) {
                    view.showSosContactList(contactsList);
                    view.hideProgressDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                if(view != null) {
                    view.hideProgressDialog();
                }
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.CONTACTS)
                .addValueEventListener(postListener);
    }

    void deleteContact(final SosContacts sosContacts) {


        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.CONTACTS)
                .child(sosContacts.getSosContactId());
        mDatabase.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    view.onSuccess("Conatact deleted");
                } else {
                    view.onError("Conatact cannot be deleted");
                }
            }
        });
    }

    /**
     * Save trip to database
     */
    void saveContactToDb(final SosContacts sosContacts) {

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.CONTACTS).push();
        mDatabase.setValue(sosContacts).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    view.onSuccess("Contact added");
                } else {
                    view.onError("Cannot add contact");
                }
            }
        });
    }

    void removeView(){
        view = null;
    }

}
