package com.autogrid.drivemode.sos;

import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;


public class Overlay extends ContextThemeWrapper implements KeyEvent.Callback, ComponentCallbacks {
	private static int loadTheme(Context context) {
		try {
			PackageManager packman =  context.getPackageManager();
			ApplicationInfo info = packman.getApplicationInfo(context.getPackageName(), 0);
			return info.theme;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return android.R.style.Theme;
	}

	private final Handler handler;

	protected OnKeyListener listener;

	private View root;
	private boolean showing;

	private Overlay parent;
	private int requestCode;
	private int resultCode;
	private Intent resultData;

	private boolean finishing;

    /**
     * This default value will be used when any one using Overlay class but don't set window flag or window type.
     * This default values use in Antitheft block screen etc. If you want to set your window flag or type then use constructor.
     */
    private static final int DEFAULT_WINDOW_FLAG = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
            | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
    private static final int DEFAULT_WINDOW_TYPE = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
    private int windowFlags;
    private int windowType;
	private int softInputMode ;

    public Overlay(Context context) {
		this(context, false);
	}

	public Overlay(Context context , int softInputMode) {
		this(context, false);
		this.softInputMode = softInputMode;
	}

	public Overlay(Context context, Overlay parent) {
		this(context, false);
		this.parent = parent;
	}

	public Overlay(Overlay parent, int requestCode) {
		this(parent);

		this.parent = parent;
		this.requestCode = requestCode;
	}

	public Overlay(Context context, boolean prioritize) {
        this(context, DEFAULT_WINDOW_TYPE, DEFAULT_WINDOW_FLAG, prioritize);
	}

    /**
     * Constructor
     * @param context {@link Context}
     * @param windowType window type Example: {@link WindowManager.LayoutParams#TYPE_SYSTEM_ERROR}
     * @param windowFlags window flag Example : {@link WindowManager.LayoutParams#FLAG_NOT_TOUCH_MODAL}
     * @param prioritize initialization priority
     */
    public Overlay(Context context, int windowType, int windowFlags, boolean prioritize) {
        super(context, loadTheme(context));
        this.windowType = windowType;
        this.windowFlags = windowFlags;

        requestCode = -1;
        handler = new Handler(context.getMainLooper());
        listener = new KeyListenerImpl(this);

        if (prioritize) {
            new InitAction(this).run();
        } else {
            handler.post(new InitAction(this));
        }
    }


	public void finish() {
		handler.post(new DeinitAction(this));
	}

	public void dismiss() {
		finish();
	}

	public void show() {
		handler.post(new ShowAction(this));
	}

	public void hide() {
		handler.post(new HideAction(this));
	}

	public boolean isShowing() {
		return showing;
	}

	private void doShow() {
		if (showing) {
			return;
		}

		showing = true;

		if (root == null) {
			return;
		}

		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
		LayoutParams old = root.getLayoutParams();

		if (old == null) {
			params.width = LayoutParams.WRAP_CONTENT;
			params.height = LayoutParams.WRAP_CONTENT;
		} else if (old instanceof WindowManager.LayoutParams) {
			params.copyFrom((WindowManager.LayoutParams) old);
		} else {
			params.width = old.width;
			params.height = old.height;
			params.layoutAnimationParameters = old.layoutAnimationParameters;
		}
		params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

		params.type = windowType;
		params.flags |= windowFlags;
		params.softInputMode |= softInputMode;
		params.format = PixelFormat.TRANSPARENT;
		try {
			getWindowManager().addView(root, params);
		}catch (SecurityException se){
			se.printStackTrace();
			showing = false;
		}catch (Exception e){
			e.printStackTrace();
			showing = false;
		}
	}

	private void doHide() {
		if (!showing) {
			return;
		}

		showing = false;

		if (root == null) {
			return;
		}
		try {
			getWindowManager().removeView(root);
		}catch (SecurityException se){
			se.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	protected void onCreate(Bundle savedInstanceState) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		registerComponentCallbacks(this);
	}

	protected void onStart() {
	}

	protected void onResume() {
	}

	protected void onPause() {
	}

	protected void onStop() {
	}

	protected void onDestroy() {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		unregisterComponentCallbacks(this);
	}

	public boolean isFinishing() {
		return finishing;
	}

	public final void runOnUiThread(Runnable action) {
		handler.post(action);
	}

	public View findViewById(int id) {
		if (root == null) {
			return null;
		}
		return root.findViewById(id);
	}

	public void setContentView(int layoutResID) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(layoutResID, new FrameLayout(this), false);
		setContentView(view);
	}

	public void setContentView(View view) {
		setKeyListeners(root, listener);
		if (showing) {
			doHide();
			root = view;
			doShow();
		} else {
			root = view;
		}
		setKeyListeners(root, listener);
	}

	public WindowManager getWindowManager() {
		return (WindowManager) getSystemService(WINDOW_SERVICE);
	}

	public void onBackPressed() {
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (getApplicationInfo().targetSdkVersion >= Build.VERSION_CODES.ECLAIR) {
				event.startTracking();
			} else {
				onBackPressed();
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		return false;
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		return false;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (getApplicationInfo().targetSdkVersion >= Build.VERSION_CODES.ECLAIR) {
			if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking() && !event.isCanceled()) {
				onBackPressed();
				return true;
			}
		}
		return false;
	}

	public static final void setKeyListeners(View view, OnKeyListener listener) {
		if (view == null) {
			return;
		} else if (!(view instanceof ViewGroup)) {
			view.setOnKeyListener(listener);
			return;
		}

		view.setOnKeyListener(listener);
		ViewGroup group = (ViewGroup) view;
		for (int i = 0; i < group.getChildCount(); i++) {
			setKeyListeners(group.getChildAt(i), listener);
		}
	}

	private static class KeyListenerImpl implements OnKeyListener {
		private KeyEvent.Callback callback;
		private KeyEvent.DispatcherState state;

		KeyListenerImpl(KeyEvent.Callback callback) {
			this.callback = callback;

			state = new KeyEvent.DispatcherState();
		}

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			event.dispatch(callback, state, callback);
			return false;
		}
	}

	private static final class InitAction implements Runnable {
		private Overlay overlay;

		InitAction(Overlay overlay) {
			this.overlay = overlay;
		}

		@Override
		public void run() {
			overlay.onCreate(new Bundle());
			overlay.onStart();
			overlay.onResume();
			overlay.doShow();
		}
	}

	private static final class DeinitAction implements Runnable {
		private Overlay overlay;

		DeinitAction(Overlay overlay) {
			this.overlay = overlay;
		}

		@Override
		public void run() {
			overlay.finishing = true;

			overlay.doHide();
			overlay.onPause();
			overlay.onStop();
			overlay.onDestroy();
			overlay.root = null;

			if (overlay.parent != null) {
				overlay.parent.onOverlayResult(overlay.requestCode, overlay.resultCode, overlay.resultData);
			}
		}
	}

	private static final class HideAction implements Runnable {
		private Overlay overlay;

		HideAction(Overlay overlay) {
			this.overlay = overlay;
		}

		@Override
		public void run() {
			overlay.doHide();
		}
	}

	private static final class ShowAction implements Runnable {
		private Overlay overlay;

		ShowAction(Overlay overlay) {
			this.overlay = overlay;
		}

		@Override
		public void run() {
			overlay.doShow();
		}
	}

	@Deprecated
	@Override
	public void startActivity(Intent intent) {
		super.startActivity(intent);
	}

	public void setResult(int resultCode) {
		this.resultCode = resultCode;
	}

	public void setResult(int resultCode, Intent data) {
		this.resultCode = resultCode;
		this.resultData = data;
	}

	protected void onOverlayResult(int requestCode, int resultCode, Intent data) {
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	}

	@Override
	public void onLowMemory() {
	}
}
