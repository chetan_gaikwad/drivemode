package com.autogrid.drivemode.sos;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.customfonts.MyTextViewOpenSansBold;
import com.autogrid.drivemode.customfonts.MyTextViewOpenSansRegular;
import com.autogrid.drivemode.customfonts.MyTextViewOpenSansSemibold;
import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.db.entity.SosContacts;
import com.autogrid.drivemode.util.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ng.max.slideview.SlideView;
import timber.log.Timber;


public class CleanerOverlay extends Overlay {

    private TextView dismiss;
    private LinearLayout sos1, sos2, sos3;
    private MyTextViewOpenSansRegular tvAllergies, tvOngoingMedications;
    private MyTextViewOpenSansRegular tvAccidentalHistory, tvInsuranceCoName, tvPolicyNumber;
    private MyTextViewOpenSansSemibold tvUserName, tvBloodGroup, tvAge, tvWeight, labelHealthInsuranceInfo;
    private MyTextViewOpenSansBold tvLabelCoveredBy, tvLabelPolicyNumber;

    private SharedPreferences preferences;
    private ImageView profilePic;
    private SlideView slideView;
    private Context context;

    private static final int flags =
             WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
            ;

    public CleanerOverlay(final Context context) {
        super(context, WindowManager.LayoutParams.TYPE_SYSTEM_ERROR, flags, true);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        (((DriveModeApp)getApplicationContext())).getDriveModeComponent().inject(this);
        setContentView(R.layout.activity_cleaner_overlay);
        dismiss = (TextView) findViewById(R.id.dismiss);
        sos1 = (LinearLayout) findViewById(R.id.ll_sos_1);
        sos1.setOnClickListener(onClickListener);
        sos2 = (LinearLayout) findViewById(R.id.ll_sos_2);
        sos2.setOnClickListener(onClickListener);
        sos3 = (LinearLayout) findViewById(R.id.ll_sos_3);
        sos3.setOnClickListener(onClickListener);

        profilePic = (ImageView) findViewById(R.id.img_banner);
        tvAccidentalHistory = (MyTextViewOpenSansRegular) findViewById(R.id.tv_accidental_history);
        tvAllergies = (MyTextViewOpenSansRegular) findViewById(R.id.tv_allergies);
        tvOngoingMedications = (MyTextViewOpenSansRegular) findViewById(R.id.tv_ongoing_medications);
        tvAge = (MyTextViewOpenSansSemibold) findViewById(R.id.tv_age);
        tvWeight = (MyTextViewOpenSansSemibold) findViewById(R.id.txtWeight);
        tvUserName = (MyTextViewOpenSansSemibold) findViewById(R.id.tv_user_name);
        tvBloodGroup = (MyTextViewOpenSansSemibold) findViewById(R.id.txtBloodGrp);

        tvInsuranceCoName = (MyTextViewOpenSansRegular) findViewById(R.id.tv_insurance_co_name);
        tvPolicyNumber = (MyTextViewOpenSansRegular) findViewById(R.id.tv_policy_number);
        labelHealthInsuranceInfo = (MyTextViewOpenSansSemibold) findViewById(R.id.label_health_insurance_info);
        tvLabelCoveredBy = (MyTextViewOpenSansBold) findViewById(R.id.tv_label_covered_by);
        tvLabelPolicyNumber = (MyTextViewOpenSansBold) findViewById(R.id.tv_label_policy_number);

        slideView = (SlideView) findViewById(R.id.slideView);

        slideView.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                finish();
            }
        });

        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                initUi();
                return 0;
            }

            @Override
            protected void onPostExecute(Integer integer) {

                super.onPostExecute(integer);
            }
        }.execute();
    }

    private void initUi(){

        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        tvUserName.setText(currentUser.getDisplayName());
        fetchSosContacts();
        addProfileDataChangedListener();

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        Log.d("dwd","sds");
    }

    View.OnClickListener onClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("dsds","calling");
        }
    };


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("dsds","onKeyDown");

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
        {
            if (event.getAction() == KeyEvent.ACTION_DOWN  &&  event.getRepeatCount() == 0)
            {
                Log.d("dsds","KEYCODE_BACK");
                return true;

            }

            else if (event.getAction() == KeyEvent.ACTION_UP)
            {
                Log.d("dsds","ACTION_UP");

            }
        }
        return super.onKeyDown(keyCode, event);
    }

    void fetchSosContacts(){
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d("Contacts %s", dataSnapshot.toString());
                List<SosContacts> contactsList = new ArrayList<>();
                for (DataSnapshot contactSnapshot: dataSnapshot.getChildren()) {
                    SosContacts sosContacts = contactSnapshot.getValue(SosContacts.class);
                    sosContacts.setSosContactId(contactSnapshot.getKey());
                    contactsList.add(sosContacts);
                }
                showSosContactList(contactsList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.CONTACTS)
                .addValueEventListener(postListener);
    }

    private void showSosContactList(List<SosContacts> userContacts) {

        if(userContacts.size() >= 1){
            MyTextViewOpenSansSemibold tvName = (MyTextViewOpenSansSemibold) sos1.findViewWithTag("tv_name");
            tvName.setText(userContacts.get(0).getSosName());
            MyTextViewOpenSansSemibold tvNumber = (MyTextViewOpenSansSemibold) sos1.findViewWithTag("tv_number");
            tvNumber.setText(userContacts.get(0).getSosNumber());
            sos1.setVisibility(View.VISIBLE);
        }if(userContacts.size() >= 2){
            MyTextViewOpenSansSemibold tvName = (MyTextViewOpenSansSemibold) sos2.findViewWithTag("tv_name");
            tvName.setText(userContacts.get(1).getSosName());
            MyTextViewOpenSansSemibold tvNumber = (MyTextViewOpenSansSemibold) sos2.findViewWithTag("tv_number");
            tvNumber.setText(userContacts.get(1).getSosNumber());
            sos2.setVisibility(View.VISIBLE);
        }if(userContacts.size() >= 3){
            MyTextViewOpenSansSemibold tvName = (MyTextViewOpenSansSemibold) sos3.findViewWithTag("tv_name");
            tvName.setText(userContacts.get(2).getSosName());
            MyTextViewOpenSansSemibold tvNumber = (MyTextViewOpenSansSemibold) sos3.findViewWithTag("tv_number");
            tvNumber.setText(userContacts.get(2).getSosNumber());
            sos3.setVisibility(View.VISIBLE);
        }

        /*customNotification(
                1,
                userContacts.size() >=1 ? "SOS: "+userContacts.get(0).getSosName()+"-"+userContacts.get(0).getSosNumber():"Please add sos contacts",
                userContacts.size() >= 2 ? "SOS: "+userContacts.get(1).getSosName()+"-"+userContacts.get(1).getSosNumber():"",
                userContacts.size() >= 3 ? "SOS: "+userContacts.get(2).getSosName()+"-"+userContacts.get(2).getSosNumber():"");*/

    }


    void addProfileDataChangedListener() {

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d(dataSnapshot.toString());
                Profile profile = dataSnapshot.getValue(Profile.class);
                    updateMedicalInfo(profile);

                /*customNotification(
                        0,
                        profile.getName(),
                        profile.getBloodGroup(),
                        profile.getOnGoingMedications());*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.PROFILE)
                .addValueEventListener(postListener);
    }

    private void updateMedicalInfo(Profile profile) {

        if(!profile.getOnGoingMedications().isEmpty()) {
            tvOngoingMedications.setText(profile.getOnGoingMedications());
        }

        if(!profile.getAccidentalHistory().isEmpty()) {
            tvAccidentalHistory.setText(profile.getAccidentalHistory());
        }

        if(!profile.getAllergies().isEmpty()) {
            tvAllergies.setText(profile.getAllergies());
        }

        if(profile.getAge() > 0) {
            tvAge.setText(String.format(Locale.US, "%d",profile.getAge()));
        }
        if(profile.getWeight() > 0) {
            //tvWeight.setText(String.format(Locale.US,"%d",profile.getWeight()+" kg"));
            tvWeight.setText(profile.getWeight()+" kg");
        }
        if(!profile.getBloodGroup().isEmpty()) {
            tvBloodGroup.setText(profile.getBloodGroup());
        }
        if(!profile.getName().isEmpty()) {
            tvUserName.setText(profile.getName());
        }
        if(profile.getInsuranceCoName() == null ||profile.getInsuranceCoName().equals("") || profile.getPolicyNumber() == null || profile.getPolicyNumber().equals("")) {
            /** Hide view*/

            tvPolicyNumber.setVisibility(View.GONE);
            tvInsuranceCoName.setVisibility(View.GONE);
            tvLabelPolicyNumber.setVisibility(View.GONE);
            tvLabelCoveredBy.setVisibility(View.GONE);
            labelHealthInsuranceInfo.setVisibility(View.GONE);

        } else {
            tvInsuranceCoName.setText(profile.getInsuranceCoName());
            tvPolicyNumber.setText(profile.getPolicyNumber());
        }

        /** Internet connection check */
        if(!Utils.isOnline(getApplicationContext())) {

            try {
                Bitmap profilePic = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(profile.getLocalImagePath() ));
                this.profilePic.setImageBitmap(profilePic);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return;
        } else {
            updateProfile(profile);
        }
    }

    public void updateProfile(Profile profile) {

        Picasso.with(getApplicationContext())
                .load(profile.getBannerImageUrl())
                .placeholder(R.drawable.people_face)
                .resize(100,100)
                .into(profilePic);
    }

    public void customNotification(int notificationId, String title, String data, String data3) {
        Log.i("customNotification","Screen went OFF. Showing custom notification");
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.noti_dialog);


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", title);
        intent.putExtra("text", data);
        intent.putExtra("text2", data3);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.mipmap.new_icon)
                // Set Ticker Message
                .setTicker("Sticker")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews)
                .setOngoing(true);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.image,R.mipmap.new_icon);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title,title);
        remoteViews.setTextViewText(R.id.text,data);
        remoteViews.setTextViewText(R.id.text2,data3);


        // Create Notification Manager
        NotificationManager notificationmanager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(notificationId, builder.build());

    }
}

