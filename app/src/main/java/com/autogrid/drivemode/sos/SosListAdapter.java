package com.autogrid.drivemode.sos;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.SosContacts;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

class SosListAdapter extends RecyclerView.Adapter<SosListAdapter.ViewHolder> {

    private List<SosContacts> sosContactList;
    private int lastPosition = -1;
    private SosActivity activity;
    private ViewHolder viewHolder;
    public SosListAdapter(SosActivity activity, List<SosContacts> trips) {
        this.sosContactList = trips;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.row_sos_list, viewGroup, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int position) {

        //setting data to view holder elements
        // populate row widgets from record data
        final SosContacts trip = sosContactList.get(position);
        viewHolder.tvTripName.setText(trip.getSosName());

        //Trip delete handle
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.deleteTrip(trip);
            }
        });

        viewHolder.cardView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return (null != sosContactList ? sosContactList.size() : 0);
    }

    /**
     * View holder to display each RecylerView item
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        // get widgets from the view
        @BindView(R.id.tvSosName)
        TextView tvTripName;
        @BindView(R.id.image_delete)
        ImageView imgDelete;
        @BindView(R.id.sosCardView)
        CardView cardView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}