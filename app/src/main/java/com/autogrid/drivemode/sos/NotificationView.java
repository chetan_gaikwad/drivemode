package com.autogrid.drivemode.sos;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.autogrid.drivemode.R;

public class NotificationView extends Activity {
	String title;
	String text;
	String text2;
	TextView txttitle;
	TextView txttext;
	TextView txtText2;
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.noti_dialog);
 
		// Create Notification Manager
		NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Dismiss Notification
		notificationmanager.cancel(0);
 
		// Retrive the data from MainActivity.java
		Intent i = getIntent();
 
		title = i.getStringExtra("title");
		text = i.getStringExtra("text");
		text2 = i.getStringExtra("text2");

        Log.d("ds","Data 3 - "+text2);
		// Locate the TextView
		txttitle = (TextView) findViewById(R.id.title);
		txttext = (TextView) findViewById(R.id.text);
		txtText2 = (TextView) findViewById(R.id.text2);

		// Set the data into TextView
		txttitle.setText(title);
		txttext.setText(text);
		txtText2.setText(text2);
	}
}