package com.autogrid.drivemode.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.Toast;

import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.util.BusProvider;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import timber.log.Timber;

import static android.R.attr.bitmap;

/**
 * Created by chetan_g on 22/11/16.
 */

class ProfilePresenter {

    private ProfileActivity view;
    ProfilePresenter(ProfileActivity view) {
        this.view = view;
    }

    void addProfileDataChangedListener() {

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d(dataSnapshot.toString());
                Profile profile = dataSnapshot.getValue(Profile.class);
                if(profile != null && view != null) {
                    view.updateUi(profile);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.PROFILE)
                .addValueEventListener(postListener);
    }

    /**Upload banner image to firebase storage server
     * @param bitmap
     */
    void uploadBannerImage(Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        StorageReference mountainsRef = storageRef.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.PROFILE)
                .child(Utils.BANNER_IMAGE)
                .child("banner.png")
                ;

        final ProgressDialog progressDialog = new ProgressDialog(view);
        progressDialog.setTitle("Uploading");
        progressDialog.show();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Timber.d("Cannot upload %s",exception.getMessage());
                progressDialog.dismiss();

                view.onError(exception.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Timber.d("Uploaded image url %s",taskSnapshot.getDownloadUrl().toString());

                //dismissing the progress dialog
                progressDialog.dismiss();

                view.onSuccess("Image uploaded");

                DatabaseReference mDatabase;
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child(Utils.APP_NAME)
                        .child(Utils.USERS)
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child(Utils.PROFILE)
                        .child("bannerImageUrl").setValue(taskSnapshot.getDownloadUrl().toString());
            }
        }) .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                //displaying the upload progress
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
            }
        });
    }

    void removeView(){
        view = null;
    }
}
