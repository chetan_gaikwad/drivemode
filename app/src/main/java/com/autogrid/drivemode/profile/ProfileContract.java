package com.autogrid.drivemode.profile;

import com.autogrid.drivemode.db.entity.Profile;

/**
 * Created by chetan_g on 22/11/16.
 */

interface ProfileContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();

    void updateUi(Profile profile);
}
