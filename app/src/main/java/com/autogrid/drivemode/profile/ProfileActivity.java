package com.autogrid.drivemode.profile;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.sos.SosActivity;
import com.autogrid.drivemode.user_signin.UserSignInActivity;
import com.autogrid.drivemode.util.BusProvider;
import com.autogrid.drivemode.util.CustomDialogClass;
import com.autogrid.drivemode.util.PreferenceUtil;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements ProfileContract
        , GoogleApiClient.OnConnectionFailedListener{

    private static final int PICK_BANNER_IMAGE = 201;

    private static final int REQUEST_CODE_OVERLAY = 2;
    @Inject
    SharedPreferences preferences;

    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.tv_user_name_profile)
    TextView txtNameProfile;
    @BindView(R.id.txt_mob_no)
    TextView txtMobNo;
    @BindView(R.id.txt_age)
    TextView txtAge;
    @BindView(R.id.txt_blood_group)
    TextView txtBloodGroup;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.txt_allergies)
    TextView txtAllergies;
    @BindView(R.id.txt_on_going_medications)
    TextView txtOnGoingMedications;
    @BindView(R.id.txt_accidental_history)
    TextView txtAccidentalHistory;
    @BindView(R.id.txt_insurece_co_name)
    TextView txtInsuranceCoName;
    @BindView(R.id.txt_policy_number)
    TextView txtPolicyNumber;

    @BindView(R.id.img_banner)
    CircleImageView imgBanner;

    @BindView(R.id.sw_lock_screen)
    Switch swLockScreen;

    @BindView(R.id.sw_notification)
    Switch swNotification;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]
    private GoogleApiClient mGoogleApiClient;

    private Profile profile;

    private ProfilePresenter profilePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ((DriveModeApp)getApplication()).getDriveModeComponent().inject(this);
        ButterKnife.bind(this);
        profilePresenter = new ProfilePresenter(this);

        swLockScreen.setChecked(preferences.getBoolean(PreferenceUtil.LOCK_WIDGET_PREFERENCE, false));
        swNotification.setChecked(preferences.getBoolean(PreferenceUtil.NOTIFICATION_WIDGET_PREFERENCE, false));

        profilePresenter.addProfileDataChangedListener();
        BusProvider.getInstance().register(this);

        if(getSupportActionBar() != null){
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(0);
        }

        swLockScreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                preferences.edit().putBoolean(PreferenceUtil.LOCK_WIDGET_PREFERENCE, isChecked).commit();
                if(isChecked){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(ProfileActivity.this)) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getApplicationContext().getPackageName()));
                            startActivityForResult(intent, REQUEST_CODE_OVERLAY);
                        }
                    }

                    /** This code creating problem #ChetanHelp*/

                    /** Show alert window */
                    new AlertDialog.Builder(ProfileActivity.this)
                            .setTitle("Experimental Feature")
                            .setMessage("Some phones do not support lock screen as overlay. Uncheck feature if it causes any problem(s).").show();
/*
                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());

                    // 2. Chain together various setter methods to set the dialog characteristics
                    builder.setMessage("Message")
                            .setTitle("Title");

                    // 3. Get the AlertDialog from create()
                    AlertDialog dialog = builder.create();
                    dialog.show();*/

                }
            }
        });

        swNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                preferences.edit().putBoolean(PreferenceUtil.NOTIFICATION_WIDGET_PREFERENCE, isChecked).commit();
                if(isChecked) {
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(ProfileActivity.this)) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getApplicationContext().getPackageName()));
                            startActivityForResult(intent, REQUEST_CODE_OVERLAY);
                        }
                    }*/
                }
            }
        });

        //Google login
        mAuth = FirebaseAuth.getInstance();
        // [START config_signin]

        FirebaseUser currentUser = mAuth.getCurrentUser();
        txtEmail.setText(currentUser.getEmail() == null ? "":currentUser.getEmail());

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    public void logOut() {
        preferences.edit().clear().commit();//Clear all preferences
        Intent intent = new Intent(this, UserSignInActivity.class);
        intent.putExtra("finish", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // To clean up all activities
        startActivity(intent);
        finish();//Clear all previous activities

        signOut();//Signout firebase
    }

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();;
        } else if(id == R.id.action_logout){

            CustomDialogClass cdd=new CustomDialogClass(this);
            cdd.show();

            //startActivity(new Intent(ProfileActivity.this, SettingActivity.class));
        } else if(id == R.id.action_add_contacts){
            startActivity(new Intent(ProfileActivity.this, SosActivity.class));
        }else if(id == R.id.action_update_app){
            UpdateApp();
        }
        return super.onOptionsItemSelected(item);
    }

    private void UpdateApp() {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.

        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
        {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else
        {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }

        goToMarket.addFlags(flags);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        Toast.makeText(this, successMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startActivity() {

    }

    @Override
    public void updateUi(Profile profile) {
        this.profile = profile;
        txtName.setText(profile.getName() == null? "":profile.getName());
        txtEmail.setText(profile.getEmail());

        if(profile.getDateOfBirth() != null){
            txtAge.setText(""+Utils.getAge(profile.getDateOfBirth()));
        }
        txtBloodGroup.setText(profile.getBloodGroup());
        txtNameProfile.setText(profile.getName() == null? "":profile.getName());
        txtAccidentalHistory.setText(profile.getAccidentalHistory() == null ? "___":profile.getAccidentalHistory());
        txtAllergies.setText(profile.getAllergies() == null ? "___":profile.getAllergies());
        txtOnGoingMedications.setText(profile.getOnGoingMedications() == null ? "___":profile.getOnGoingMedications());

        txtInsuranceCoName.setText(profile.getInsuranceCoName() == null ? "___":profile.getInsuranceCoName());
        txtPolicyNumber.setText(profile.getPolicyNumber() ==null? "":profile.getPolicyNumber());

        /** Check mob no */
        txtMobNo.setVisibility(View.GONE);

        updateProfile(profile);
    }

    public void updateProfile(Profile profile) {

        Picasso.with(ProfileActivity.this)
                .load(profile.getBannerImageUrl())
                .placeholder(R.drawable.people_face)
                .into(imgBanner);
    }

    @OnClick({R.id.txtEditProfile, R.id.img_banner})
    public void onClick(View view){
        if (view.getId() == R.id.txtEditProfile){
            Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
            intent.putExtra(Utils.PROFILE_OBJECT, profile);
            startActivity(intent);
        } else if (view.getId() == R.id.img_banner){
            pickImage(PICK_BANNER_IMAGE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profilePresenter.removeView();
        BusProvider.getInstance().unregister(this);
    }

    /**
     * Pick image intent
     * @param action
     */
    private void pickImage(int action) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, action);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_BANNER_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                //Todo Display an error
                return;
            }

            Uri uri = data.getData();


            if (uri != null) {
                Bitmap uploadBitmap;
                Bitmap newProfilePic = null;
                try {
                    newProfilePic = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }

                if(newProfilePic.getHeight() > 96 || newProfilePic.getWidth() > 96){
                    uploadBitmap = Bitmap.createScaledBitmap(newProfilePic, 96, 96, false);
                } else {
                    uploadBitmap = newProfilePic;
                }
                imgBanner.setImageBitmap(uploadBitmap);

                profile.setLocalImagePath(uri.getPath());

                /** Internet connection check */
                if(!Utils.isOnline(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                profilePresenter.uploadBannerImage(uploadBitmap);
            }
        }
    }
}


