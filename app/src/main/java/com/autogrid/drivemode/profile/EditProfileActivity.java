package com.autogrid.drivemode.profile;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;


public class EditProfileActivity extends BaseActivity {


    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtBloogGroup)
    EditText edtBloodGroup;

    //@BindView(R.id.spBloodGroup);
    Spinner spBloodGroup;

    @BindView(R.id.edt_allergies)
    EditText edtAllergies;
    @BindView(R.id.edt_medical_problems)
    EditText edtOnGoingMedications;
    @BindView(R.id.edt_surgeries_and_implants)
    EditText edtAccidentalHistory;
    @BindView(R.id.edt_birth_date)
    EditText edtBirthDate;
    @BindView(R.id.edtWeight)
    EditText edtWeight;
    @BindView(R.id.edt_insurance_co_name)
    EditText edtInsuranceCompName;
    @BindView(R.id.edt_policy_number)
    EditText edtPolicyNumber;

    @BindView(R.id.btn_save)
    TextView save;

    private Profile profile;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        spBloodGroup = (Spinner) findViewById(R.id.spBloodGroup);
        List<String> bloodGroupsList = new ArrayList<>();
        bloodGroupsList.add("A +ve");
        bloodGroupsList.add("A -ve");
        bloodGroupsList.add("B +ve");
        bloodGroupsList.add("B -ve");
        bloodGroupsList.add("AB +ve");
        bloodGroupsList.add("AB -ve");
        bloodGroupsList.add("O +ve");
        bloodGroupsList.add("O -ve");

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        profile = getIntent().getParcelableExtra(Utils.PROFILE_OBJECT);
        if(profile != null){
            edtAccidentalHistory.setText(profile.getAccidentalHistory());
            edtAllergies.setText(profile.getAllergies());
            edtBloodGroup.setText(profile.getBloodGroup());
            try {
                spBloodGroup.setSelection(bloodGroupsList.indexOf(profile.getBloodGroup()) );
            } catch (Exception e) {

            }
            edtBirthDate.setText(profile.getDateOfBirth());
            edtWeight.setText(String.format(Locale.US,"%02.02f",profile.getWeight()));
            edtName.setText(profile.getName());
            edtOnGoingMedications.setText(profile.getOnGoingMedications());
            edtInsuranceCompName.setText(profile.getInsuranceCoName());
            edtPolicyNumber.setText(profile.getPolicyNumber());
        }

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edtBirthDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        edtBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.btn_save})
    public void onClick(final View view){

        /** Internet connection check */
        if(!Utils.isOnline(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();
        }

        /** Button disable  */
        view.setEnabled(false);

        String onGoingMedication = edtOnGoingMedications.getText().toString();
        String name = edtName.getText().toString();

        String bloodGroup = spBloodGroup.getSelectedItem().toString();
        String accidentalHistory = edtAccidentalHistory.getText().toString();
        String allergies = edtAllergies.getText().toString();
        String weight = edtWeight.getText().toString();
        String age = edtBirthDate.getText().toString();
        String dateOfBirth = edtBirthDate.getText().toString();

        if(onGoingMedication.isEmpty() &&
                name.isEmpty() &&
                bloodGroup.isEmpty() &&
                accidentalHistory.isEmpty() &&
                allergies.isEmpty() ){
            Toasty.error(EditProfileActivity.this, "All fields are empty").show();

            /** Button enable  */
            view.setEnabled(true);
        } else {
            FirebaseAuth mAuth;
            mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();

            Profile profile = new Profile();
            profile.setName(edtName.getText().toString());
            if (currentUser.getEmail() != null) {
                profile.setEmail(currentUser.getEmail());
            }
            profile.setBloodGroup(bloodGroup);
            profile.setDateOfBirth(dateOfBirth);
            profile.setWeight(weight.isEmpty()? 0: Float.parseFloat(weight));

            profile.setAllergies(edtAllergies.getText().toString());
            profile.setOnGoingMedications(edtOnGoingMedications.getText().toString());
            profile.setAccidentalHistory(edtAccidentalHistory.getText().toString());

            profile.setInsuranceCoName( edtInsuranceCompName.getText().toString().trim());
            profile.setPolicyNumber(edtPolicyNumber.getText().toString().trim());

            if (this.profile != null) {
                profile.setBannerImageUrl(this.profile.getBannerImageUrl());
            }
            if (this.profile != null) {
                profile.setCoverImageUrl(this.profile.getCoverImageUrl());
            }
            DatabaseReference mDatabase;
            mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(Utils.APP_NAME)
                    .child(Utils.USERS)
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(Utils.PROFILE)
                    .setValue(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    /** Button enable  */
                    view.setEnabled(true);

                    if (task.isSuccessful()) {
                        Toasty.success(EditProfileActivity.this, "Information updated").show();
                    } else {
                        Toasty.error(EditProfileActivity.this, "Error while updating information").show();
                    }
                }
            });
        }
    }
}
