package com.autogrid.drivemode.manage_vehicle.view_vehicle;

import com.autogrid.drivemode.db.entity.ReportVehicle;
import com.autogrid.drivemode.util.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by chetan_g on 22/11/16.
 */

class ViewReportPresenter {

    private ViewReportActivity view;
    ViewReportPresenter(ViewReportActivity view) {
        this.view = view;
    }

    void removeView(){
        view = null;
    }


    public void fetchAllContactsFromDatabase() {

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        view.showProgressDialog();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d("Contacts %s", dataSnapshot.toString());
                List<ReportVehicle> contactsList = new ArrayList<>();
                for (DataSnapshot contactSnapshot: dataSnapshot.getChildren()) {
                    ReportVehicle sosContacts = contactSnapshot.getValue(ReportVehicle.class);
                    sosContacts.setId(contactSnapshot.getKey());
                    contactsList.add(sosContacts);
                }

                if(view != null) {
                    view.showSosContactList(contactsList);
                    view.hideProgressDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                if(view != null) {
                    view.hideProgressDialog();
                }
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.CONTACTS)
                .addValueEventListener(postListener);
    }
}
