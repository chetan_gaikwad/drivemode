package com.autogrid.drivemode.manage_vehicle.add_vehicle;

/**
 * Created by chetan_g on 22/11/16.
 */

interface AddVehicleContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
}
