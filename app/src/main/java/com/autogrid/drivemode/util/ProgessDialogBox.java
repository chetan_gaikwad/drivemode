package com.autogrid.drivemode.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

public class ProgessDialogBox {
	private ProgressDialog progressDialog;
    Activity activity;

	public ProgessDialogBox(Activity activity) {
		progressDialog = new ProgressDialog(activity);
        this.activity = activity;
	}

	public ProgessDialogBox(Context activity) {
		progressDialog = new ProgressDialog(activity);
	}

	public void showDailog() {
		progressDialog.setMessage("Please wait...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show();

	}

	public void setMessage(String message) {
		progressDialog.setMessage(message);
	}

	public void showDailog(String msg) {

		progressDialog.setMessage(msg);
		progressDialog.setIndeterminate(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show();

	}
    public void showPercentageDailog(String msg,int max) {
        progressDialog.setMessage(msg);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(max);
        progressDialog.show();

    }
	public void setProgress(final int progress){
        progressDialog.setProgress(progress);
	}
	public void dismissDialog() {
		progressDialog.dismiss();
	}
	public boolean isShowing() {
		return progressDialog.isShowing();
	}
}
