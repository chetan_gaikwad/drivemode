package com.autogrid.drivemode.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by 638 on 3/10/20n17.
 */

public class Utils {
    public static final String LAT = "lat";
    public static final String LONGI = "longi";
    public static final String TRIP_NAME = "trip_name";
    public static final String APP_NAME = "SmartRide";
    public static final String USERS = "Users";
    public static final String FEATURE_REQUEST = "Feature_Requests";
    public static final String PROFILE = "Profile";
    public static final String REPORTS = "Reports";
    public static final String CONTACTS = "Contacts";
    public static final String TRIPS = "Trips";
    public static final String BANNER_IMAGE = "Banner_Image";
    public static final String COVER_IMAGE = "Cover_Image";
    public static final String IS_TRIP_LIVE = "Is_Trip_Live";
    public static final String TRIP_OBJECT = "trip_object";
    public static final String PROFILE_OBJECT = "profile_object";

    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    /**
     * get date in yyyy-MM-dd HH:mm:ss format
     * @return
     */
    public static String getTimeMicro() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return sdf.format(c.getTime());
    }

    public static int getAge(String dateStr){

        SimpleDateFormat format =new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date date = null;
        try {
            date = format.parse(dateStr);
            Calendar dob = Calendar.getInstance();
            dob.setTime(date);
            Calendar today = Calendar.getInstance();

            //dob.set(date.get(Calendar.YEAR), date.getMonth(), date.getDate());

            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
                age--;
            }
            Integer ageInt = new Integer(age);
            return ageInt;

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
