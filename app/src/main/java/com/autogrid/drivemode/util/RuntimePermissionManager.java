package com.autogrid.drivemode.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by 638 on 25-Jun-17.
 */

public class RuntimePermissionManager {

    /**
     * Generic method to check permission status
     * @param permissionName
     */
    public static boolean isPermissionAllowed(Context context, String permissionName) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(context, permissionName);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    /**
     * Generic method to request permission
     * @param permissionName
     * @param requestCode
     */
    public static void requestPermission(Activity context, String permissionName, int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context,permissionName)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            /*Snackbar
                    .make(findViewById(android.R.id.content), "Permission was denied " +
                            "earlier, Please enable it from app settings", Snackbar.LENGTH_LONG)
                    .show();*/

        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(context,new String[]{permissionName},requestCode);
    }

}
