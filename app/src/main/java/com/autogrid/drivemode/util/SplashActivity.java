package com.autogrid.drivemode.util;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.autogrid.drivemode.user_signin.UserSignInActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, UserSignInActivity.class);
        startActivity(intent);
        finish();
    }
}
