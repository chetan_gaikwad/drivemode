package com.autogrid.drivemode.util;

/**
 * Created by 638 on 2/11/2017.
 */

public class PreferenceUtil {
    public static final String LOGIN_PREFERENCE = "login_preference";
    public static final String LOCK_WIDGET_PREFERENCE = "lock_screen_widget";
    public static final String NOTIFICATION_WIDGET_PREFERENCE = "notification_widget";

    public static final String FCM_TOKEN_PREF = "fcm_token_preference";
    public static final String USER_ID_PREF = "user_id_pref";
    public static final String ICON_CREDIT_PREF = "icon_credit";
    public static final String OBD_DEVICE_PREF = "obd_device";
}
