package com.autogrid.drivemode.dashboard;

import com.autogrid.drivemode.db.entity.Profile;

/**
 * Created by chetan_g on 22/11/16.
 */

interface DashboardContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
     void updateProfile(Profile profile);
}
