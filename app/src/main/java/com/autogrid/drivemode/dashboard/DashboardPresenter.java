package com.autogrid.drivemode.dashboard;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.widget.EditText;

import com.autogrid.drivemode.autobot.RemoteControlReceiver;
import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.manage_trips.live_trip.LiveTripActivity;
import com.autogrid.drivemode.services.LocationManagerService;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import timber.log.Timber;

/**
 * Created by chetan_g on 22/11/16.
 */

class DashboardPresenter {

    private MainActivity view;
    private SharedPreferences preferences;
    DashboardPresenter(MainActivity view, SharedPreferences preferences) {
        this.view = view;
        this.preferences = preferences;
    }

    void addProfileDataChangedListener() {

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d(dataSnapshot.toString());
                Profile profile = dataSnapshot.getValue(Profile.class);
                if(profile != null) {
                    view.updateProfile(profile);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.PROFILE)
                .addValueEventListener(postListener);
    }

    /**
     * Start a service which will provice location update throughtout the app
     */
    void startLocationService() {

        /** Creating dialoge box to get trip name */
        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(view);
        final EditText input = new EditText(view);
        alert.setView(input);
        alert.setTitle("Please enter trip name");
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String tripName = input.getText().toString().trim();

                Intent sensorService = new Intent(view, LocationManagerService.class);
                sensorService.putExtra(Utils.TRIP_NAME, tripName);
                view.startService(sensorService);
                preferences.edit().putBoolean(Utils.IS_TRIP_LIVE, true).apply();
                preferences.edit().putString(Utils.TRIP_NAME, tripName).commit();

                view.startActivity(new Intent(view, LiveTripActivity.class));
            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        alert.show();


    }


    void registerHeadphoneButtonClickService() {
        AudioManager mAudioManager =  (AudioManager) view.getSystemService(Context.AUDIO_SERVICE);
        ComponentName mReceiverComponent = new ComponentName(view,RemoteControlReceiver.class);
        mAudioManager.registerMediaButtonEventReceiver(mReceiverComponent);
    }

    void unRegisterHeadphoneButtonClickService() {
        AudioManager mAudioManager =  (AudioManager) view.getSystemService(Context.AUDIO_SERVICE);
        ComponentName mReceiverComponent = new ComponentName(view,RemoteControlReceiver.class);
        mAudioManager.unregisterMediaButtonEventReceiver(mReceiverComponent);
    }

    /**Upload banner image to firebase storage server
     * @param bitmap
     */
    void uploadBannerImage(Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        StorageReference mountainsRef = storageRef.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.PROFILE)
                .child(Utils.BANNER_IMAGE)
                .child("banner.png")
                ;

        final ProgressDialog progressDialog = new ProgressDialog(view);
        progressDialog.setTitle("Uploading");
        progressDialog.show();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Timber.d("Cannot upload %s",exception.getMessage());
                progressDialog.dismiss();

                view.onError(exception.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Timber.d("Uploaded image url %s",taskSnapshot.getDownloadUrl().toString());

                //dismissing the progress dialog
                progressDialog.dismiss();

                view.onSuccess("Image uploaded");

                DatabaseReference mDatabase;
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child(Utils.APP_NAME)
                        .child(Utils.USERS)
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child(Utils.PROFILE)
                        .child("bannerImageUrl").setValue(taskSnapshot.getDownloadUrl().toString());
            }
        }) .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                //displaying the upload progress
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
            }
        });
    }

    void removeView(){
        view = null;
    }
}
