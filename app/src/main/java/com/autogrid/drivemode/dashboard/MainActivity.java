package com.autogrid.drivemode.dashboard;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Profile;
import com.autogrid.drivemode.db.entity.Trip;
import com.autogrid.drivemode.manage_trips.trip_list.TripListActivity;
import com.autogrid.drivemode.profile.ProfileActivity;
import com.autogrid.drivemode.report_vehicle.report.ReportVehicleActivity;
import com.autogrid.drivemode.request_feature.RequestFeatureActivity;
import com.autogrid.drivemode.settings.SettingActivity;
import com.autogrid.drivemode.sos.SosActivity;
import com.autogrid.drivemode.user_signin.UserSignInActivity;
import com.autogrid.drivemode.util.BusProvider;
import com.autogrid.drivemode.util.RuntimePermissionManager;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements DashboardContract {

    private static final int PICK_COVER_IMAGE = 200;
    private static final int PICK_BANNER_IMAGE = 201;
    private static final int BLUETOOTH_PERMISSION_CODE = 202;
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 203;
    private static final int LOCATION_PERMISSION_CODE = 204;

    private DashboardPresenter dashboardPresenter;


    @Inject
    SharedPreferences preferences;
    @BindView(R.id.img_banner)
    CircleImageView imgBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        boolean finish = getIntent().getBooleanExtra("finish", false);
        if (finish) {
            startActivity(new Intent(this, UserSignInActivity.class));
            finish();
            return;
        }


        ((DriveModeApp)getApplication()).getDriveModeComponent().inject(this);
        ButterKnife.bind(this);
        dashboardPresenter = new DashboardPresenter(this, preferences);

        if(RuntimePermissionManager.isPermissionAllowed(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            initializeLocation();
        } else {
            RuntimePermissionManager.requestPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_PERMISSION_CODE);
        }
        dashboardPresenter.addProfileDataChangedListener();


        dashboardPresenter.registerHeadphoneButtonClickService();
        BusProvider.getInstance().register(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit_profile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            return true;
        } else if(id == R.id.action_request_feature){
            startActivity(new Intent(MainActivity.this, RequestFeatureActivity.class));
            return true;
        }else if(id == R.id.action_report_vehicle){
            startActivity(new Intent(MainActivity.this, ReportVehicleActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static int getCallState(Context context) {
        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        int callState = telManager.getCallState();
        switch (callState) {
            case TelephonyManager.CALL_STATE_OFFHOOK:
                return TelephonyManager.CALL_STATE_OFFHOOK;
            case TelephonyManager.CALL_STATE_RINGING:
                return TelephonyManager.CALL_STATE_RINGING;
            case TelephonyManager.CALL_STATE_IDLE:
                return TelephonyManager.CALL_STATE_IDLE;
            default:
                return 10000;
        }
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if(requestCode == LOCATION_PERMISSION_CODE){

            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                initializeLocation();
            }else{
                //Displaying another toast if permission is not granted
                Timber.d("LOCATION permission denied");
            }

            requestBluetoothPermission();

        } else if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO){
            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                dashboardPresenter.startLocationService();

            }else{
                Timber.d("RECORD_AUDIO permission denied");
            }
        }
    }

    private void requestBluetoothPermission() {
        if(RuntimePermissionManager.isPermissionAllowed(this, Manifest.permission.BLUETOOTH)) {
            Timber.d("Bluetooth permission given");
        } else {
            RuntimePermissionManager.requestPermission(this, Manifest.permission.BLUETOOTH, BLUETOOTH_PERMISSION_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Initialize location
     */
    private void initializeLocation() {
    }

    @Subscribe
    public void getTrip(Trip trip){
        showToast("Trip stopped");
        ((TextView)findViewById(R.id.tv_trip_data)).setText(trip.toString());
    }
    @Override
    public void onSuccess(String successMessage) {

    }

    @Override
    public void onError(String errorMessage) {

    }

    @Override
    public void startActivity() {

    }

    @Override
    public void updateProfile(Profile profile) {

        Picasso.with(MainActivity.this)
                .load(profile.getBannerImageUrl())
                .placeholder(R.drawable.people_face)
                .into(imgBanner);
    }

    /**
     * Start a new trip
     * @param view
     */
    public void startTrip(View view) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    PERMISSIONS_REQUEST_RECORD_AUDIO);
        } else {

            dashboardPresenter.startLocationService();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);//Unregister event bus

        dashboardPresenter.unRegisterHeadphoneButtonClickService();//Unregister headphone click listener
        dashboardPresenter.removeView();//Unregister activity view
    }

    /**
     * Handle all on clicks
     * @param view
     */
    @OnClick({R.id.ll_new_ride, R.id.ll_settings, R.id.ll_sos_contacts, R.id.ll_view_rides,
    R.id.img_banner})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.ll_new_ride:
                //Start new ride
                startTrip(view);
                break;
            case R.id.ll_settings:
                //View settings
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
                break;
            case R.id.ll_sos_contacts:
                //View Sos contacts
                startActivity(new Intent(MainActivity.this, SosActivity.class));
                break;
            case R.id.ll_view_rides:
                //View my previous rides
                startActivity(new Intent(MainActivity.this, TripListActivity.class));
                break;
            case R.id.img_banner:
                //Update banner image
                pickImage(PICK_BANNER_IMAGE);
                break;
            default:break;
        }
    }


    /**
     * Pick image intent
     * @param action
     */
    private void pickImage(int action) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
        startActivityForResult(chooserIntent, action);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_BANNER_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                //Todo Display an error
                return;
            }

            Uri uri = data.getData();

            if (uri != null) {
                Bitmap uploadBitmap;
                Bitmap newProfilePic = null;
                try {
                    newProfilePic = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }

                if(newProfilePic.getHeight() > 96 || newProfilePic.getWidth() > 96){
                    uploadBitmap = Bitmap.createScaledBitmap(newProfilePic, 96, 96, false);
                } else {
                    uploadBitmap = newProfilePic;
                }
                imgBanner.setImageBitmap(uploadBitmap);
                dashboardPresenter.uploadBannerImage(uploadBitmap);
            }
        }
    }
}
