package com.autogrid.drivemode.report_vehicle.report;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.ReportVehicle;
import com.autogrid.drivemode.services.RestService;
import com.autogrid.drivemode.util.Utils;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;


public class ReportVehicleActivity extends BaseActivity implements ReportVehicleContract{


    @BindView(R.id.edt_vehicle_number)
    EditText edtVehicleNumber;
    @BindView(R.id.edt_report_type)
    EditText edtReportType;
    @BindView(R.id.btn_report)
    Button btnReport;
    ReportVehiclePresenter reportVehiclePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_vehicle);
        ButterKnife.bind(this);
        reportVehiclePresenter = new ReportVehiclePresenter(this);
    }

    @OnClick(R.id.btn_report)
    public void onViewClicked() {
        String reportType = edtReportType.getText().toString();
        String vehicleNumber = edtVehicleNumber.getText().toString();
        if(reportType.isEmpty() && vehicleNumber.isEmpty()){
            Toasty.error(this, "Please enter all the fields").show();
        } else {
            try {
                JSONObject data = new JSONObject();
                data.put("reportType", reportType);
                data.put("vehicleNumber", vehicleNumber);

                /*reportVehiclePresenter.sendRequest(
                        new ReportVehicle(Utils.getTimeMicro(),
                                vehicleNumber.replaceAll("[^a-zA-Z]+", " "),
                                reportType,
                                "",
                                FirebaseAuth.getInstance().getCurrentUser().getEmail() == null?"":
                                        FirebaseAuth.getInstance().getCurrentUser().getEmail()
                ));*/
                //new ReportVehicleTask().execute(data);
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        Toasty.success(this, successMessage).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toasty.error(this, errorMessage).show();
    }

    private class ReportVehicleTask extends AsyncTask<JSONObject, Void, Void>{

        @Override
        protected Void doInBackground(JSONObject... objects) {
            String response = new RestService().restServiceCall("/user/sendNotificationToUser", "POST", objects[0]);
            Timber.d("Resonse %s", response);
            return null;
        }


    }
}
