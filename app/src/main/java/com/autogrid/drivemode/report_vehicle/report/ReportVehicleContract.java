package com.autogrid.drivemode.report_vehicle.report;

/**
 * Created by chetan_g on 22/11/16.
 */

interface ReportVehicleContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
}
