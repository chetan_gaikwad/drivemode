package com.autogrid.drivemode.report_vehicle.view_my_report;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.ReportVehicle;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;


public class ViewReportActivity extends BaseActivity implements ViewReportContract {

    @BindView(R.id.report_list)
    RecyclerView tripList;
    private ReportListAdapter adapter;
    private ViewReportPresenter sosPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_report);

        ButterKnife.bind(this);
        intializeTripListRecycleView();
        sosPresenter = new ViewReportPresenter(this);
        sosPresenter.fetchAllContactsFromDatabase();
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
    private void intializeTripListRecycleView() {
        tripList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        tripList.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccess(String successMessage) {
        Toasty.success(this, successMessage).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toasty.error(this, errorMessage).show();
    }

    @Override
    public void showSosContactList(List<ReportVehicle> trips) {
        adapter = new ReportListAdapter(this, trips);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tripList.setAdapter(adapter);
            }
        });
    }
}
