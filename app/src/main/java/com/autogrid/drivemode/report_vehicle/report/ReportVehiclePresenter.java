package com.autogrid.drivemode.report_vehicle.report;

import android.support.annotation.NonNull;

import com.autogrid.drivemode.db.entity.ReportVehicle;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by chetan_g on 22/11/16.
 */

class ReportVehiclePresenter {

    private ReportVehicleActivity view;
    ReportVehiclePresenter(ReportVehicleActivity view) {
        this.view = view;
    }

    void removeView(){
        view = null;
    }

    void sendRequest(ReportVehicle reportVehicle) {

        view.showProgressDialog();
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child(Utils.APP_NAME)
                .child(Utils.REPORTS)
                .child(reportVehicle.getVehicleNumber())
                .push();
        mDatabase.setValue(reportVehicle).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    view.onSuccess("Vehicle reported");
                    view.onBackPressed();
                } else {
                    view.onError("Error");
                }

                view.hideProgressDialog();
            }
        });
    }
}
