package com.autogrid.drivemode.report_vehicle.view_my_report;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.ReportVehicle;
import com.autogrid.drivemode.report_vehicle.report.ReportVehicleActivity;
import com.autogrid.drivemode.sos.SosActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ViewHolder> {


    private List<ReportVehicle> sosContactList;
    private int lastPosition = -1;

    private ViewReportActivity activity;
    private ViewHolder viewHolder;
    public ReportListAdapter(ViewReportActivity activity, List<ReportVehicle> trips) {
        this.sosContactList = trips;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.row_report_list, viewGroup, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int position) {

        //setting data to view holder elements
        // populate row widgets from record data
        final ReportVehicle trip = sosContactList.get(position);
        viewHolder.tvReportMsg.setText(trip.getReportMessage());
        viewHolder.tvReportedBy.setText(trip.getReportMessage());

        //Trip delete handle
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // activity.deleteTrip(trip);
            }
        });

        viewHolder.cardView.setTag(position);
    }




    @Override
    public int getItemCount() {
        return (null != sosContactList ? sosContactList.size() : 0);
    }

    /**
     * View holder to display each RecylerView item
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        // get widgets from the view
        @BindView(R.id.tv_report_msg)
        TextView tvReportMsg;
        @BindView(R.id.tv_report_by)
        TextView tvReportedBy;
        @BindView(R.id.image_delete)
        ImageView imgDelete;
        @BindView(R.id.sosCardView)
        CardView cardView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}