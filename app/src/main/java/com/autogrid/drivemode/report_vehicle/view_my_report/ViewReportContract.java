package com.autogrid.drivemode.report_vehicle.view_my_report;

import com.autogrid.drivemode.db.entity.ReportVehicle;

import java.util.List;

/**
 * Created by chetan_g on 22/11/16.
 */

interface ViewReportContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);

     void showSosContactList(List<ReportVehicle> trips);
}
