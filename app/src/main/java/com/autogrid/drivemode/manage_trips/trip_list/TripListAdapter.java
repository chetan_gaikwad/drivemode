package com.autogrid.drivemode.manage_trips.trip_list;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Trip;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.ViewHolder> {


    private List<Trip> trips;
    private int lastPosition = -1;

    private TripListActivity activity;
    private ViewHolder viewHolder;
    public TripListAdapter(TripListActivity activity, List<Trip> trips) {
        this.trips = trips;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.row_trip_list, viewGroup, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int position) {

        //setting data to view holder elements
        // populate row widgets from record data
        final Trip trip = trips.get(position);
        viewHolder.tvTripName.setText(trip.getTripName());

        viewHolder.tvStartTime.setText(trip.getStartTime());
        viewHolder.tvTripEndTime.setText(trip.getStopTime());

        viewHolder.tvTripDuration.setText(trip.getTotalTimeTaken());

        viewHolder.tvTripDistance.setText(
                String.format(Locale.US, "%.1fkm", trip.getTotalDistanceCovered())
        );

        viewHolder.tvAvgSpeed.setText(String.format(" | Avg.Spd %s ",trip.getAvgSpeed()));

        //Trip delete handle
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.deleteTrip(trip);
            }
        });

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.showTripDetails(trip);
            }
        });

        viewHolder.cardView.setTag(position);
    }




    @Override
    public int getItemCount() {
        return (null != trips ? trips.size() : 0);
    }

    /**
     * View holder to display each RecylerView item
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        // get widgets from the view
        @BindView(R.id.tv_trip_name)
        TextView tvTripName;
        @BindView(R.id.tv_start_time)
        TextView tvStartTime;
        @BindView(R.id.tv_end_time)
        TextView tvTripEndTime;
        @BindView(R.id.tv_trip_duration)
        TextView tvTripDuration;
        @BindView(R.id.tv_trip_distance)
        TextView tvTripDistance;
        @BindView(R.id.tv_avg_speed)
        TextView tvAvgSpeed;
        @BindView(R.id.tv_car_name)
        TextView tvCarName;
        @BindView(R.id.image_delete)
        ImageView imgDelete;
        @BindView(R.id.img_car_picture)
        ImageView imgCarPicture;
        @BindView(R.id.card_view)
        CardView cardView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}