package com.autogrid.drivemode.manage_trips.live_trip;

import com.autogrid.drivemode.db.entity.Trip;

import java.util.List;

/**
 * Created by chetan_g on 22/11/16.
 */

interface LiveTripContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
}
