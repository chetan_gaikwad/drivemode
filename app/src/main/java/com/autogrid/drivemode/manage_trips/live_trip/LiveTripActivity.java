package com.autogrid.drivemode.manage_trips.live_trip;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.autobot.Config;
import com.autogrid.drivemode.autobot.Events;
import com.autogrid.drivemode.autobot.TTS;
import com.autogrid.drivemode.db.entity.TripStatus;
import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.services.LocationManagerService;
import com.autogrid.drivemode.util.BusProvider;
import com.autogrid.drivemode.util.RuntimePermissionManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonElement;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Locale;

import javax.inject.Inject;

import ai.api.android.AIConfiguration;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.model.Status;
import ai.api.ui.AIDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

public class LiveTripActivity extends BaseActivity implements OnMapReadyCallback,
        AIDialog.AIDialogListener,
        LiveTripContract
{

    private GoogleMap mMap;
    protected static final int CALL_PERMISSION_CODE = 200;
    private AIDialog aiDialog;
    @Inject
    SharedPreferences preferences;
    @BindView(R.id.txt_distance)
    TextView txtDistance;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_speed)
    TextView txtSpeed;
    @BindView(R.id.img_bot)
    ImageView imgBot;
    @BindView(R.id.img_stop)

    ImageView imgStop;
    private LiveTripPresenter liveTripPresenter;
    private final int CONTACT_PERMISSION_CODE = 23;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        ((DriveModeApp)getApplication()).getDriveModeComponent().inject(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        BusProvider.getInstance().register(this);
        TTS.init(getApplicationContext());


        final AIConfiguration config = new AIConfiguration(Config.ACCESS_TOKEN,
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);

        aiDialog = new AIDialog(this, config);
        aiDialog.setResultsListener(this);
        liveTripPresenter = new LiveTripPresenter(this);
        liveTripPresenter.manageGpsSetting();
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if(RuntimePermissionManager.isPermissionAllowed(this, Manifest.permission.READ_CONTACTS)) {
            Timber.d("Have contact permission");
        } else {
            RuntimePermissionManager.requestPermission(this, Manifest.permission.READ_CONTACTS, CONTACT_PERMISSION_CODE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
    }

    @Subscribe
    public void headPhoneEvent(Events events){
        if(events == Events.HEADPHONE_CLICK){
            Timber.d("Got headphone event");
            aiDialog.showAndListen();
        }
    }
    @Override
    public void onResult(final AIResponse response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Timber.d( "onResult");

                //resultTextView.setText(gson.toJson(response));

                Timber.d( "Received success response");

                // this is example how to get different parts of result object
                final Status status = response.getStatus();
                Timber.d( "Status code: " + status.getCode());
                Timber.d( "Status type: " + status.getErrorType());

                final Result result = response.getResult();
                Timber.d( "Resolved query: " + result.getResolvedQuery());

                Timber.d( "Action: " + result.getAction());
                final String speech = result.getFulfillment().getSpeech();
                Timber.d( "Speech: " + speech);
                TTS.speak(speech);

                final Metadata metadata = result.getMetadata();
                if (metadata != null) {
                    Timber.d( "Intent id: " + metadata.getIntentId());
                    Timber.d( "Intent name: " + metadata.getIntentName());
                }

                final HashMap<String, JsonElement> params = result.getParameters();
                if(result.getAction().equalsIgnoreCase("call")){
                    liveTripPresenter.completeCallAction(params);
                } else if(result.getAction().contains("navigation")){
                    liveTripPresenter.completeDirectionAction(result, params);
                }
            }
        });
    }


    @Subscribe
    public void onLocationChanged(Location location){
        if(mMap != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
        }
        //txtSpeed.setText(String.format(Locale.US,"%02.01f",location.getSpeed()));
    }

    @Subscribe
    public void onTripStatus(TripStatus tripStatus){
        txtDistance.setText(tripStatus.getDistance());
        txtTime.setText(tripStatus.getTime());
        if(tripStatus.getSpeed() != null && !tripStatus.getSpeed().isEmpty()) {
            txtSpeed.setText(tripStatus.getSpeed());
        }
    }
    @Override
    public void onError(final AIError error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //resultTextView.setText(error.toString());
            }
        });
    }

    @Override
    public void onCancelled() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //resultTextView.setText("");
            }
        });
    }

    @Override
    protected void onPause() {
        if (aiDialog != null) {
            aiDialog.pause();
        }
        super.onPause();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == CONTACT_PERMISSION_CODE) {
            if(RuntimePermissionManager.isPermissionAllowed(this, Manifest.permission.CALL_PHONE)) {
                Timber.d("Have calling permission");
            } else {
                RuntimePermissionManager.requestPermission(this,
                        Manifest.permission.CALL_PHONE,
                        CALL_PERMISSION_CODE);
            }
        }
    }
        @Override
    protected void onResume() {
        if (aiDialog != null) {
            aiDialog.resume();
        }
        super.onResume();
    }

    @OnClick({R.id.img_bot, R.id.img_stop})
    public void buttonListenOnClick(final View view) {
        switch (view.getId()){
            case R.id.img_bot:
                aiDialog.showAndListen();
                break;
            case R.id.img_stop:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.app_name))
                        .setMessage(R.string.stop_trip_confirmation)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                liveTripPresenter.stopTrip();
                            }
                        }).create().show();

                break;
            default:break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }




    @Override
    public void onSuccess(String successMessage) {
        Toasty.success(this, successMessage).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toasty.error(this, errorMessage).show();
    }

    @Override
    public void startActivity() {

    }
}
