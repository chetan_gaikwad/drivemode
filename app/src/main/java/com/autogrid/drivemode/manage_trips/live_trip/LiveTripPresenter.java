package com.autogrid.drivemode.manage_trips.live_trip;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.autogrid.drivemode.R;
import com.autogrid.drivemode.services.LocationManagerService;
import com.autogrid.drivemode.util.RuntimePermissionManager;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ai.api.model.Result;
import timber.log.Timber;

/**
 * Created by chetan_g on 22/11/16.
 */

class LiveTripPresenter {

    private LiveTripActivity view;
    LiveTripPresenter(LiveTripActivity view) {
        this.view = view;
    }

    /**
     * Check whether GPS is enable, if not then prompt use to enable from settings.
     */
    protected void manageGpsSetting() {
        LocationManager lm = (LocationManager)view.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(view);
            dialog.setMessage(view.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(view.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    view.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(view.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }
    }


    /**
     * Handle call commands processed by bot
     * @param params
     */
    void completeCallAction( HashMap<String, JsonElement> params) {
        if (params != null && !params.isEmpty()) {
            Timber.d( "Parameters: ");
            for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                if(entry.getKey().equalsIgnoreCase("any")) {
                    Timber.d(String.format("%s: %s", entry.getKey(), entry.getValue().getAsString()));
                    startCall(getContactNumber(entry.getValue().getAsString()));
                }
            }
        }
    }

    /**
     * Start a phone call
     * @param contactName
     */
    private void startCall(String contactName) {

        Timber.d("Autobot calling %s", contactName);
            if(RuntimePermissionManager.isPermissionAllowed(view, Manifest.permission.CALL_PHONE)
                    && !contactName.isEmpty()) {
                Timber.d("Calling %s", contactName);
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contactName));
                view.startActivity(intent);
            } else {
                RuntimePermissionManager.requestPermission(view, Manifest.permission.CALL_PHONE, view.CALL_PERMISSION_CODE);
            }
    }

    /**
     * Search contact number by name
     * @param s
     * @return
     */
    public String getContactNumber(String s){
        Timber.d("Autobot Searching for contact %s", s);
        String number="";


        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor people = view.getContentResolver().query(uri, projection, null, null, null);

        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        people.moveToFirst();
        do {
            String Name   = people.getString(indexName);
            String Number = people.getString(indexNumber);
            if(Name.equalsIgnoreCase(s)){
                Timber.d("Autobot Found number %s", Number);
                return Number.replace("-", "");
            }
            // Do work...
        } while (people.moveToNext());


        if(!number.equalsIgnoreCase("")){return number.replace("-", "");}
        else return number;
    }


    /**
     * Handle navigation commands processed by bot
     * @param result
     * @param params
     */
     void completeDirectionAction(Result result, HashMap<String, JsonElement> params) {

        if(result.getAction().equalsIgnoreCase("navigation.start")) {
            if (params != null && !params.isEmpty()) {
                Timber.d("Parameters: ");
                for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase("to")) {

                        try {
                            Timber.d(String.format("%s : %s", entry.getKey(), entry.getValue()));
                            if(entry.getValue().isJsonObject()) {
                                JSONObject locationJson = new JSONObject(entry.getValue().toString());
                                startDirection("", locationJson.getString("city"));
                            } else {
                                startDirection("", entry.getValue().toString());
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                }
            }
        }
    }


    /**
     * Start navigation to givenLocation
     * @param fromLocation
     * @param toLocation
     */
    private void startDirection(String fromLocation, String toLocation) {
        view.onSuccess(String.format(Locale.US,"Navigating to %s", toLocation));
    }

    void stopTrip() {
        Intent sensorService = new Intent(view, LocationManagerService.class);
        view.stopService(sensorService);
        view.finish();
    }
}
