package com.autogrid.drivemode.manage_trips.trip_details;

/**
 * Created by chetan_g on 22/11/16.
 */

interface TripDetailContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
}
