package com.autogrid.drivemode.manage_trips.trip_list;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Trip;
import com.autogrid.drivemode.DriveModeApp;
import com.autogrid.drivemode.manage_trips.trip_details.TripDetailActivity;
import com.autogrid.drivemode.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class TripListActivity extends BaseActivity implements TripListContract{

    @BindView(R.id.tripList)
    RecyclerView tripList;
    private TripListAdapter adapter;


    private TripListPresenter tripListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_list);
        ((DriveModeApp)getApplication()).getDriveModeComponent().inject(this);
        ButterKnife.bind(this);
        intializeTripListRecycleView();
        tripListPresenter = new TripListPresenter(this);
        tripListPresenter.fetchAllTripsFromDatabase();

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();;
        }
        return super.onOptionsItemSelected(item);
    }

    private void intializeTripListRecycleView() {
        tripList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        tripList.setLayoutManager(layoutManager);
    }

    @Override
    public void onSuccess(String successMessage) {
        Toasty.success(this, successMessage).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toasty.error(this, errorMessage).show();
    }

    @Override
    public void startActivity() {

    }

    @Override
    public void showTripList(List<Trip> trips) {
        adapter = new TripListAdapter(TripListActivity.this, trips);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tripList.setAdapter(adapter);
            }
        });
    }

    @Override
    public void showTripDetails(Trip trip) {
        Intent intent = new Intent(this, TripDetailActivity.class);
        intent.putExtra(Utils.TRIP_OBJECT, trip);
        startActivity(intent);
    }

    public void deleteTrip(final Trip trip) {

        new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Do you want delete this trip?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        tripListPresenter.deleteTrip(trip);
                        adapter.notifyDataSetChanged();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tripListPresenter.removeView();
    }
}
