package com.autogrid.drivemode.manage_trips.trip_details;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.autogrid.drivemode.BaseActivity;
import com.autogrid.drivemode.R;
import com.autogrid.drivemode.db.entity.Trip;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;


public class TripDetailActivity extends BaseActivity implements OnMapReadyCallback,TripDetailContract {

    private GoogleMap mMap;
    @BindView(R.id.txt_start_loc)
    TextView txtStartLocation;
    @BindView(R.id.txt_stop_loc)
    TextView txtStopLocation;
    @BindView(R.id.txt_start_time)
    TextView txtStartTime;
    @BindView(R.id.txt_stop_time)
    TextView txtStopTime;
    @BindView(R.id.txt_time_taken)
    TextView txtTimetake;
    @BindView(R.id.txt_distance_covered)
    TextView txtDistanceCovered;
    @BindView(R.id.txt_avg_speed)
    TextView txtAverageSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Trip trip = getIntent().getParcelableExtra(Utils.TRIP_OBJECT);
        Timber.d(trip.toString());

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(trip.getTripName());
        }



        txtStartLocation.setText(trip.getStartAddress());
        txtStopLocation.setText(trip.getStopAddress());
        txtStartTime.setText(String.format(Locale.US, "  %s",trip.getStartTime()));
        txtStopTime.setText(String.format(Locale.US, "  %s",trip.getStopTime()));
        txtTimetake.setText(trip.getTotalTimeTaken());
        txtDistanceCovered.setText(String.format(Locale.US,"%02.02f km",trip.getTotalDistanceCovered()));
        txtAverageSpeed.setText(String.format(Locale.US,"%s km/hr",trip.getAvgSpeed()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackPressed();;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    @Override
    public void onSuccess(String successMessage) {

    }

    @Override
    public void onError(String errorMessage) {

    }

    @Override
    public void startActivity() {

    }

}
