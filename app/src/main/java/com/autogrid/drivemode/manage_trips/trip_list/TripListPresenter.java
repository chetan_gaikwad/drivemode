package com.autogrid.drivemode.manage_trips.trip_list;

import android.support.annotation.NonNull;

import com.autogrid.drivemode.db.entity.Trip;
import com.autogrid.drivemode.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by chetan_g on 22/11/16.
 */

class TripListPresenter {

    private TripListActivity view;
    TripListPresenter(TripListActivity view) {
        this.view = view;
    }

    void fetchAllTripsFromDatabase(){

        view.showProgressDialog();

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d("Contacts %s", dataSnapshot.toString());
                List<Trip> tripList = new ArrayList<>();
                for (DataSnapshot tripSnapshot: dataSnapshot.getChildren()) {
                    Trip sosContacts = tripSnapshot.getValue(Trip.class);
                    sosContacts.setTripId(tripSnapshot.getKey());
                    tripList.add(sosContacts);
                }
                if(view != null) {
                    view.showTripList(tripList);
                    view.hideProgressDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Timber.d("loadPost:onCancelled", databaseError.toException());
                // ...
                view.hideProgressDialog();
            }
        };
        mDatabase.child(Utils.APP_NAME)
                .child(Utils.USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Utils.TRIPS)
                .orderByChild("stopTime")
                .addValueEventListener(postListener);
    }

    void deleteTrip(final Trip trip) {

            view.showProgressDialog();
            DatabaseReference mDatabase;
            mDatabase = FirebaseDatabase.getInstance().getReference().child(Utils.APP_NAME)
                    .child(Utils.USERS)
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(Utils.TRIPS)
                    .child(trip.getTripId());
            mDatabase.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        view.onSuccess("Trip deleted");
                    } else {
                        view.onError("cannot delete trip");
                    }
                    view.hideProgressDialog();;
                }
            });
    }

    void removeView(){
        view = null;
    }
}
