package com.autogrid.drivemode.manage_trips.trip_list;

import com.autogrid.drivemode.db.entity.Trip;

import java.util.List;

/**
 * Created by chetan_g on 22/11/16.
 */

interface TripListContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
     void showTripList(List<Trip> trips);
     void showTripDetails(Trip trip);
}
