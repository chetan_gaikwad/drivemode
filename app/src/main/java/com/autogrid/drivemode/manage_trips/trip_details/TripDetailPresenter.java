package com.autogrid.drivemode.manage_trips.trip_details;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.autogrid.drivemode.R;
import com.autogrid.drivemode.manage_trips.live_trip.LiveTripActivity;
import com.autogrid.drivemode.util.RuntimePermissionManager;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ai.api.model.Result;
import timber.log.Timber;

/**
 * Created by chetan_g on 22/11/16.
 */

class TripDetailPresenter {

    private LiveTripActivity view;
    TripDetailPresenter(LiveTripActivity view) {
        this.view = view;
    }

}
