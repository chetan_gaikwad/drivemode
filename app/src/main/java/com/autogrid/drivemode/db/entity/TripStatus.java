package com.autogrid.drivemode.db.entity;

import android.support.annotation.Keep;

/**
 * Created by 638 on 15-Jul-17.
 */
@Keep
public class TripStatus {
    private String distance;
    private String time;
    private String speed;

    public TripStatus() {
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
