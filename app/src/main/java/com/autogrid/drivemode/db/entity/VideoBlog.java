package com.autogrid.drivemode.db.entity;

/**
 * Created by 638 on 07-Aug-17.
 */

public class VideoBlog {
    private String videoTitle;
    private String videoFrom;
    private String timeStamp;
    private String duration;
    private String videoThumbnailUrl;

    public VideoBlog(String videoTitle, String videoFrom, String timeStamp, String duration, String videoThumbnailUrl) {
        this.videoTitle = videoTitle;
        this.videoFrom = videoFrom;
        this.timeStamp = timeStamp;
        this.duration = duration;
        this.videoThumbnailUrl = videoThumbnailUrl;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoFrom() {
        return videoFrom;
    }

    public void setVideoFrom(String videoFrom) {
        this.videoFrom = videoFrom;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getVideoThumbnailUrl() {
        return videoThumbnailUrl;
    }

    public void setVideoThumbnailUrl(String videoThumbnailUrl) {
        this.videoThumbnailUrl = videoThumbnailUrl;
    }
}
