package com.autogrid.drivemode.db.entity;

import android.support.annotation.Keep;

/**
 * Created by 638 on 03-Aug-17.
 */
@Keep
public class RequestFeature {
    String dateTime;
    String email;
    String requestText;

    public RequestFeature(String dateTime, String from, String request) {
        this.dateTime = dateTime;
        this.email = from;
        requestText = request;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequestText() {
        return requestText;
    }

    public void setRequestText(String requestText) {
        this.requestText = requestText;
    }
}
