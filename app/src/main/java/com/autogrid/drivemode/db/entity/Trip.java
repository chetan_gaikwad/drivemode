package com.autogrid.drivemode.db.entity;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Keep;

/** Copyright Chetan Gaikwad
 *  Class which represent Trip table of the database
 * Created by Chetan on 20-Jun-17.
 */
@Keep
public class Trip implements Parcelable{
    private String tripId;
    private String tripName;
    private String startTime;
    private String stopTime;
    private double startLat;
    private double startLong;
    private double stopLat;
    private double stopLong;
    private float totalDistanceCovered;
    private String totalTimeTaken;
    private int maxSpeed;
    private String startAddress;
    private String stopAddress;



    private String avgSpeed;

    public Trip() {
    }

    protected Trip(Parcel in) {
        tripId = in.readString();
        tripName = in.readString();
        startTime = in.readString();
        stopTime = in.readString();
        startLat = in.readDouble();
        startLong = in.readDouble();
        stopLat = in.readDouble();
        stopLong = in.readDouble();
        totalDistanceCovered = in.readFloat();
        totalTimeTaken = in.readString();
        maxSpeed = in.readInt();
        startAddress = in.readString();
        stopAddress = in.readString();
        avgSpeed = in.readString();
    }

    public static final Creator<Trip> CREATOR = new Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public double getStartLat() {
        return startLat;
    }

    public void setStartLat(double startLat) {
        this.startLat = startLat;
    }

    public double getStartLong() {
        return startLong;
    }

    public void setStartLong(double startLong) {
        this.startLong = startLong;
    }

    public double getStopLat() {
        return stopLat;
    }

    public void setStopLat(double stopLat) {
        this.stopLat = stopLat;
    }

    public double getStopLong() {
        return stopLong;
    }

    public void setStopLong(double stopLong) {
        this.stopLong = stopLong;
    }

    public float getTotalDistanceCovered() {
        return totalDistanceCovered;
    }

    public void setTotalDistanceCovered(float totalDistanceCovered) {
        this.totalDistanceCovered = totalDistanceCovered;
    }

    public String getTotalTimeTaken() {
        return totalTimeTaken;
    }

    public void setTotalTimeTaken(String totalTimeTaken) {
        this.totalTimeTaken = totalTimeTaken;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getStopAddress() {
        return stopAddress;
    }

    public void setStopAddress(String stopAddress) {
        this.stopAddress = stopAddress;
    }

    public String getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(String avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "tripId=" + tripId +
                ", tripName='" + tripName + '\'' +
                ", startTime='" + startTime + '\'' +
                ", stopTime='" + stopTime + '\'' +
                ", startLat=" + startLat +
                ", startLong=" + startLong +
                ", stopLat=" + stopLat +
                ", stopLong=" + stopLong +
                ", totalDistanceCovered=" + totalDistanceCovered +
                ", totalTimeTaken='" + totalTimeTaken + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", startAddress='" + startAddress + '\'' +
                ", stopAddress='" + stopAddress + '\'' +
                ", avgSpeed='" + avgSpeed + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tripId);
        parcel.writeString(tripName);
        parcel.writeString(startTime);
        parcel.writeString(stopTime);
        parcel.writeDouble(startLat);
        parcel.writeDouble(startLong);
        parcel.writeDouble(stopLat);
        parcel.writeDouble(stopLong);
        parcel.writeFloat(totalDistanceCovered);
        parcel.writeString(totalTimeTaken);
        parcel.writeInt(maxSpeed);
        parcel.writeString(startAddress);
        parcel.writeString(stopAddress);
        parcel.writeString(avgSpeed);
    }
}
