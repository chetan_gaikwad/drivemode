package com.autogrid.drivemode.db.entity;

import android.support.annotation.Keep;

/**
 * Created by 638 on 04-Sep-17.
 */
@Keep
public class ReportVehicle {
    private String timestamp;
    private String vehicleNumber;
    private String reportMessage;
    private String location;
    private String reportedBy;
    private String id;

    public ReportVehicle(String timestamp, String vehicleNumber, String reportMessage, String location, String reportedBy, String id) {
        this.timestamp = timestamp;
        this.vehicleNumber = vehicleNumber;
        this.reportMessage = reportMessage;
        this.location = location;
        this.reportedBy = reportedBy;
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getReportMessage() {
        return reportMessage;
    }

    public String getLocation() {
        return location;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
