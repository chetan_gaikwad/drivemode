package com.autogrid.drivemode.db.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Keep;

/**
 * Created by 638 on 11-Jul-17.
 */
@Keep
public class Profile implements Parcelable{
    private String name;
    private String email;
    private String coverImageUrl;
    private String bannerImageUrl;
    private String bloodGroup;
    private String allergies;
    private String onGoingMedications;
    private String accidentalHistory;
    private int age;
    private float weight;
    private String localImagePath;
    private String dateOfBirth;
    private String insuranceCoName;
    private String policyNumber;

    public Profile() {
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getBannerImageUrl() {
        return bannerImageUrl;
    }

    public void setBannerImageUrl(String bannerImageUrl) {
        this.bannerImageUrl = bannerImageUrl;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getOnGoingMedications() {
        return onGoingMedications;
    }

    public void setOnGoingMedications(String onGoingMedications) {
        this.onGoingMedications = onGoingMedications;
    }

    public String getAccidentalHistory() {
        return accidentalHistory;
    }

    public void setAccidentalHistory(String accidentalHistory) {
        this.accidentalHistory = accidentalHistory;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    protected Profile(Parcel in) {
        name = in.readString();
        email = in.readString();
        coverImageUrl = in.readString();
        bannerImageUrl = in.readString();
        bloodGroup = in.readString();
        allergies = in.readString();
        onGoingMedications = in.readString();
        accidentalHistory = in.readString();
        age = in.readInt();
        weight = in.readFloat();
        dateOfBirth = in.readString();
        insuranceCoName = in.readString();
        policyNumber = in.readString();
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeString(coverImageUrl);
        parcel.writeString(bannerImageUrl);
        parcel.writeString(bloodGroup);
        parcel.writeString(allergies);
        parcel.writeString(onGoingMedications);
        parcel.writeString(accidentalHistory);
        parcel.writeInt(age);
        parcel.writeFloat(weight);
        parcel.writeString(dateOfBirth);
        parcel.writeString(insuranceCoName);
        parcel.writeString(policyNumber);
    }

    public String getLocalImagePath() {
        return localImagePath;
    }

    public void setLocalImagePath(String localImagePath) {
        this.localImagePath = localImagePath;
    }

    public String getInsuranceCoName() {
        return insuranceCoName;
    }

    public void setInsuranceCoName(String insuranceCoName) {
        this.insuranceCoName = insuranceCoName;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }
}
