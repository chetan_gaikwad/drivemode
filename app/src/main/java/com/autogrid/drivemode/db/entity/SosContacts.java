package com.autogrid.drivemode.db.entity;


import android.support.annotation.Keep;

/** Copyright Chetan Gaikwad
 *  Class which represent SosContacts table of the database
 * Created by Chetan on 1-July-17.
 */
@Keep
public class SosContacts {
    private String sosContactId;
    private String sosName;
    private String sosNumber;

    public SosContacts() {
    }

    public String getSosContactId() {
        return sosContactId;
    }

    public void setSosContactId(String sosContactId) {
        this.sosContactId = sosContactId;
    }

    public String getSosName() {
        return sosName;
    }

    public void setSosName(String sosName) {
        this.sosName = sosName;
    }

    public String getSosNumber() {
        return sosNumber;
    }

    public void setSosNumber(String sosNumber) {
        this.sosNumber = sosNumber;
    }

    @Override
    public String toString() {
        return "SosContacts{" +
                "sosContactId=" + sosContactId +
                ", sosName='" + sosName + '\'' +
                ", sosNumber='" + sosNumber + '\'' +
                '}';
    }
}
