package com.autogrid.drivemode.user_signin;

/**
 * Created by chetan_g on 22/11/16.
 */

interface UserSignInContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
     void startActivity();
}
